package com.coherentlogic.quandl.client.core.converters.timeseries.data;

import com.coherentlogic.quandl.client.core.domain.timeseries.metadata.Frequency;
import com.thoughtworks.xstream.converters.enums.EnumSingleValueConverter;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class FrequencyEnumConverter extends EnumSingleValueConverter {

    public FrequencyEnumConverter(Class<? extends Enum> type) {
        super(type);
    }

    @Override
    public Object fromString(String value) {
        return (value == null || "".equals (value)) ? null : Frequency.valueOf(value);
    }
}
