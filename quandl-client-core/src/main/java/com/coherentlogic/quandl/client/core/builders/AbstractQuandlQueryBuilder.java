package com.coherentlogic.quandl.client.core.builders;

import javax.ws.rs.core.UriBuilder;

import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.adapter.core.builders.ApiKeyRequiredSpecification;
import com.coherentlogic.coherent.data.adapter.core.builders.rest.AbstractRESTQueryBuilder;
import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.adapter.core.command.CommandExecutorSpecification;
import com.coherentlogic.coherent.data.adapter.core.util.WelcomeMessage;

/**
 * Builder implementation which all other Quandl-related builders extend from.
 *
 * @see <a href="https://docs.quandl.com/">Quandl API Documentation</a>
 * @see <a href="https://blog.quandl.com/getting-started-with-the-quandl-api">Getting Started with the Quandl
 *  API</a>
 * @see <a href="https://docs.quandl.com/docs/parameters-2">Times-series parameters</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 */
public class AbstractQuandlQueryBuilder extends AbstractRESTQueryBuilder<String>
    implements ApiKeyRequiredSpecification {

    static final String[] WELCOME_MESSAGE = {
        " Quandl Client version 2.0.3-RELEASE"
    };

    static {

        WelcomeMessage welcomeMessage = new WelcomeMessage();

        for (String next : WELCOME_MESSAGE) {
            welcomeMessage.addText(next);
        }

        welcomeMessage.display();
    }

    public static final String DEFAULT_URI = "https://www.quandl.com/api/v3/";

    public AbstractQuandlQueryBuilder(RestTemplate restTemplate) {
        super(restTemplate, DEFAULT_URI);
    }

    public AbstractQuandlQueryBuilder(
        RestTemplate restTemplate,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, DEFAULT_URI, commandExecutor);
    }

    public AbstractQuandlQueryBuilder(
        RestTemplate restTemplate,
        CacheServiceProviderSpecification<String> cache,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, DEFAULT_URI, cache, commandExecutor);
    }

    public AbstractQuandlQueryBuilder(RestTemplate restTemplate, String uri) {
        super(restTemplate, uri);
    }

    public AbstractQuandlQueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, uri, commandExecutor);
    }

    public AbstractQuandlQueryBuilder(RestTemplate restTemplate, UriBuilder uriBuilder) {
        super(restTemplate, uriBuilder);
    }

    public AbstractQuandlQueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, uri, cache);
    }

    public AbstractQuandlQueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, uriBuilder, cache);
    }

    public AbstractQuandlQueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<String> cache,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, uri, cache, commandExecutor);
    }

    public AbstractQuandlQueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<String> cache,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, uriBuilder, cache, commandExecutor);
    }

    @Override
    protected String getKey() {
        return getEscapedURI();
    }

    @Override
    protected <T> T doExecute(Class<T> type) {
        return (T) getRestTemplate ().getForObject(getEscapedURI (), type);
    }

    static final String API_KEY = "api_key", QUANDL_API_KEY = "QUANDL_API_KEY";

    /**
     * Method sets the key using either the VM argument with the QUANDL_API_KEY key -- if this returns null then the
     * system environment variable with key QUANDL_API_KEY is checked -- if this also returns null then an exception is
     * thrown.
     *
     * @see {@link com.coherentlogic.coherent.data.adapter.core.builders.ApiKeyRequiredSpecification}
     */
    public <T> T withExternalApiKey () {
        return withExternalApiKey (QUANDL_API_KEY);
    }

    /**
     * Method sets the key using either the VM argument with the QUANDL_API_KEY key -- if this returns null then the
     * system environment variable with key QUANDL_API_KEY is checked -- if this also returns null then an exception is
     * thrown.
     *
     * @see {@link com.coherentlogic.coherent.data.adapter.core.builders.ApiKeyRequiredSpecification}
     */
    public <T> T withExternalApiKey (String key) {
        return (T) withApiKey(getProperty(key));
    }

    /**
     * @see {@link com.coherentlogic.coherent.data.adapter.core.builders.ApiKeyRequiredSpecification}
     */
    public <T> T withApiKey (String apiKey) {
        return (T) addParameter(API_KEY, apiKey);
    }

    protected AbstractQuandlQueryBuilder withXML (String value) {

        extendPathWith (notNull ("value", value).concat(".xml"));

        return this;
    }

    /**
     * This method adds "metadata.xml" to the URI as demonstrated in the following URL:
     *
     * https://www.quandl.com/api/v3/datasets/WIKI/FB/metadata.xml?api_key=YOURAPIKEY
     */
    public AbstractQuandlQueryBuilder metadata () {
        return (AbstractQuandlQueryBuilder) extendPathWith("metadata.xml");
    }

    /**
     * This method adds "data.xml" to the URI as demonstrated in the following URL:
     *
     * https://www.quandl.com/api/v3/datasets/WIKI/FB/data.xml?api_key=YOURAPIKEY
     */
    public AbstractQuandlQueryBuilder data () {
        return (AbstractQuandlQueryBuilder) extendPathWith("data.xml");
    }
}
