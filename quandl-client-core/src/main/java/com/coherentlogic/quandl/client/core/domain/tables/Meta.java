package com.coherentlogic.quandl.client.core.domain.tables;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.quandl.client.core.converters.NillableStringValueReflectionConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 * <next-cursor-id nil="true"/>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@XStreamAlias (Meta.META_ALIAS)
public class Meta extends SerializableBean {

    private static final long serialVersionUID = 2390325368072537739L;

    public static final String META_ALIAS = "meta", NEXT_CURSOR_ID_ALIAS = "next-cursor-id";

    @XStreamAlias (NEXT_CURSOR_ID_ALIAS)
    @XStreamConverter (NillableStringValueReflectionConverter.class)
    private String nextCursorId = null;

    public String getNextCursorId() {
        return nextCursorId;
    }

    public void setNextCursorId(String nextCursorId) {
        this.nextCursorId = nextCursorId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((nextCursorId == null) ? 0 : nextCursorId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Meta other = (Meta) obj;
        if (nextCursorId == null) {
            if (other.nextCursorId != null)
                return false;
        } else if (!nextCursorId.equals(other.nextCursorId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Meta [nextCursorId=" + nextCursorId + "]";
    }
}
