package com.coherentlogic.quandl.client.core.domain.timeseries;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public enum Transform {

    /**
     * @deprecated Used when Quandl returns null or "", may be changed to none in the future if deemed
     *  appropriate.
     */
    nullOrBlank,
    none,
    diff,
    rdiff,
    rdiff_from,
    cumul,
    normalize
}
