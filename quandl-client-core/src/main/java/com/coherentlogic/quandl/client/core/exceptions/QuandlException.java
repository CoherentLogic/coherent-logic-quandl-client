package com.coherentlogic.quandl.client.core.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * 
 * @see <a href="https://docs.quandl.com/docs/error-codes">Quandl Error Codes</a>
 * 
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class QuandlException extends NestedRuntimeException {

    private static final long serialVersionUID = 6806008233331486968L;

    private final int errorCode;

    public QuandlException(int errorCode) {
        this ("The call to Quandl failed (errorCode: " + errorCode + ")", errorCode);
    }

    public QuandlException(String msg, int errorCode) {

        super(msg);

        this.errorCode = errorCode;
    }

    public QuandlException(String msg, int errorCode, Throwable thrown) {

        super(msg, thrown);

        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
