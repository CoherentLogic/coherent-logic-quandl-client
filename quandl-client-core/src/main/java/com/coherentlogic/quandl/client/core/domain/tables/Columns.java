package com.coherentlogic.quandl.client.core.domain.tables;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.coherent.data.model.core.strategies.TraversalStrategySpecification;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@XStreamAlias(Columns.COLUMNS)
public class Columns extends SerializableBean {

    private static final long serialVersionUID = 9191263331235463428L;

    public static final String COLUMNS = "columns", TYPE = "type";

    @XStreamAlias(TYPE)
    private String type;

    @XStreamImplicit
    @XStreamAlias(Column.COLUMN)
    private List<Column> columnList;

    private static final TraversalStrategySpecification defaultTraversalStrategy
        = new TraversalStrategySpecification () {

        @Override
        public void execute(SerializableBean serializableBean, Collection<Consumer<SerializableBean>> visitors) {

            Columns columns = (Columns) serializableBean;

            if (columns.columnList != null)
                columns.columnList.forEach(column -> { column.accept(visitors); });

            visitors.forEach(visitor -> { visitor.accept(columns); } );
        }
    };

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {
        accept(defaultTraversalStrategy, visitors);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Column> getColumnList() {
        return columnList;
    }

    public void setColumnList(List<Column> columnList) {
        this.columnList = columnList;
    }

    public Map<String, Column> asMap () {

        final Map<String, Column> result = new HashMap<String, Column> (columnList.size ());

        columnList.forEach(
            column -> {
                result.put(column.getName (), column);
            }
        );

        return result;
    }

    public Column getColumnAtIndex (int index) {

        int size = columnList.size();

        if (size < index)
            throw new IndexOutOfBoundsException("The index (" + index + ") is greater than the size (" + size
                + ") of the column list.");

        Column result = columnList.get(index);

        return result;
    }

    /**
     * 
     * @param column
     * @return
     *
     * @TODO Consider adding the index as a property of the Column.
     */
    public int getIndexOf (Column column) {
        return columnList.indexOf(column);
    }

    public int size () {
        return columnList.size ();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((columnList == null) ? 0 : columnList.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Columns other = (Columns) obj;
        if (columnList == null) {
            if (other.columnList != null)
                return false;
        } else if (!columnList.equals(other.columnList))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Columns [type=" + type + ", columnList=" + columnList + "]";
    }
}
