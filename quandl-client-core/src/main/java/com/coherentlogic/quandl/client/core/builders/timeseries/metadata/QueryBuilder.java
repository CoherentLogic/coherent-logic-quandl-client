package com.coherentlogic.quandl.client.core.builders.timeseries.metadata;

import java.util.function.Function;

import javax.ws.rs.core.UriBuilder;

import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.quandl.client.core.builders.AbstractQuandlQueryBuilder;
import com.coherentlogic.quandl.client.core.builders.timeseries.AbstractTimeSeriesQueryBuilder;
import com.coherentlogic.quandl.client.core.domain.timeseries.metadata.QuandlResponse;

/**
 * Implementation of the {@link AbstractTimeSeriesQueryBuilder} for accessing the Quandl metadata API.
 *
 * @see <a href="https://docs.quandl.com/">Quandl API Documentation</a>
 * @see <a href="https://blog.quandl.com/getting-started-with-the-quandl-api">Getting Started with the Quandl
 * API</a>
 * @see <a href="https://docs.quandl.com/docs/parameters-2">Times-series parameters</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class QueryBuilder extends AbstractTimeSeriesQueryBuilder<QueryBuilder, QuandlResponse> {

    public QueryBuilder(RestTemplate restTemplate) {
        super(restTemplate, AbstractQuandlQueryBuilder.DEFAULT_URI);
    }

    public QueryBuilder(RestTemplate restTemplate, String uri) {
        super(restTemplate, uri);
    }

    public QueryBuilder(RestTemplate restTemplate, UriBuilder uriBuilder) {
        super(restTemplate, uriBuilder);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, uri, cache);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, uriBuilder, cache);
    }

    @Override
    public QueryBuilder withApiKey (String apiKey) {
        return (QueryBuilder) super.withApiKey (apiKey);
    }

    @Override
    public QueryBuilder withExternalApiKey() {
        return (QueryBuilder) super.withExternalApiKey();
    }

    @Override
    public QueryBuilder withExternalApiKey(String key) {
        return (QueryBuilder) super.withExternalApiKey(key);
    }

    /**
     * This method adds "metadata.xml" to the URI as demonstrated in the following URL:
     *
     * https://www.quandl.com/api/v3/datasets/WIKI/FB/metadata.xml?api_key=YOURAPIKEY
     */
    public QueryBuilder metadata () {
        return (QueryBuilder) extendPathWith("metadata.xml");
    }

    public QuandlResponse doGetAsQuandlResponse () {
        return doGetAsQuandlResponse (quandlResponse -> { return quandlResponse; });
    }

    /**
     * Do get as {@link QuandlResponse}, execute the given function, and then return an instance of type
     * {@link Seriess}.
     */
    public QuandlResponse doGetAsQuandlResponse (Function<QuandlResponse, QuandlResponse> function) {
        return doGetAsQuandlResponse (QuandlResponse.class, function);
    }

    /**
     * Do get as {@link QuandlResponse}, execute the given function, and then return an instance of type
     * resultType.
     */
    public <R> R doGetAsQuandlResponse (Class<R> resultType, Function<QuandlResponse, R> function) {

        QuandlResponse quandlResponse = doGet(QuandlResponse.class);

        R result = function.apply(quandlResponse);

        return result;
    }
}
