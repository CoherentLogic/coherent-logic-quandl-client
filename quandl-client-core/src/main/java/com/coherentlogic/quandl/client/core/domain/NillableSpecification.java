package com.coherentlogic.quandl.client.core.domain;

import java.io.Serializable;

/**
 * A specification for any bean which has a nil flag.
 */
public interface NillableSpecification extends Serializable {

    public Boolean getNil();
}
