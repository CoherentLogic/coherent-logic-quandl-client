package com.coherentlogic.quandl.client.core.builders.timeseries.data;

import java.util.function.Function;

import javax.ws.rs.core.UriBuilder;

import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.adapter.core.command.CommandExecutorSpecification;
import com.coherentlogic.quandl.client.core.builders.timeseries.AbstractTimeSeriesQueryBuilder;
import com.coherentlogic.quandl.client.core.domain.timeseries.data.DatasetData;
import com.coherentlogic.quandl.client.core.domain.timeseries.data.QuandlResponse;

/**
 * Builder implementation for accessing the Quandl time series API.
 *
 * @see <a href="https://docs.quandl.com/">Quandl API Documentation</a>
 * @see <a href="https://blog.quandl.com/getting-started-with-the-quandl-api">Getting Started with the Quandl
 *  API</a>
 * @see <a href="https://docs.quandl.com/docs/parameters-2">Times-series parameters</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class QueryBuilder extends AbstractTimeSeriesQueryBuilder<QueryBuilder, QuandlResponse> {

    public QueryBuilder(RestTemplate restTemplate) {
        super(restTemplate, DEFAULT_URI);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, DEFAULT_URI, commandExecutor);
    }

    public QueryBuilder(RestTemplate restTemplate, String uri) {
        super(restTemplate, uri);
    }

    public QueryBuilder(RestTemplate restTemplate, UriBuilder uriBuilder) {
        super(restTemplate, uriBuilder);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, uri, cache);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, uriBuilder, cache);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        CacheServiceProviderSpecification<String> cache,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, DEFAULT_URI, cache, commandExecutor);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<String> cache,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, uri, cache, commandExecutor);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<String> cache,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, uriBuilder, cache, commandExecutor);
    }

    /**
     * Method adds the apiKey parameter to the URI and is <i>required</i> for every call to the Quandl API.
     */
    @Override
    public QueryBuilder withApiKey (String apiKey) {
        return (QueryBuilder) super.withApiKey (apiKey);
    }

    @Override
    public QueryBuilder withExternalApiKey() {
        return (QueryBuilder) super.withExternalApiKey();
    }

    @Override
    public QueryBuilder withExternalApiKey(String key) {
        return (QueryBuilder) super.withExternalApiKey(key);
    }

    /**
     * This method adds "data.xml" to the URI as demonstrated in the following URL:
     *
     * https://www.quandl.com/api/v3/datasets/WIKI/FB/data.xml?api_key=YOURAPIKEY
     */
    public QueryBuilder data () {
        return (QueryBuilder) extendPathWith("data.xml");
    }

    /**
     * Method extends the URI with "databases".
     *
     * @see <a href="https://docs.quandl.com/docs/in-depth-usage#section-get-an-entire-time-series-dataset">
     * Get an entire time-series dataset</a>
     */
    public QueryBuilder databases () {
        return (QueryBuilder) extendPathWith("databases");
    }

    @Override
    public QuandlResponse doGetAsQuandlResponse (Function<QuandlResponse, QuandlResponse> function) {
        return doGetAsQuandlResponse (QuandlResponse.class, function);
    }

    /**
     * Do get as {@link DatasetData}, execute the given function, and then return an instance of type
     * resultType.
     */
    @Override
    public <R> R doGetAsQuandlResponse (Class<R> resultType, Function<QuandlResponse, R> function) {

        QuandlResponse quandlResponse = doGet(QuandlResponse.class);

        R result = function.apply(quandlResponse);

        return result;
    }
}
