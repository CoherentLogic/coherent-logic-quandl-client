package com.coherentlogic.quandl.client.core.domain.tables;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.coherent.data.model.core.strategies.TraversalStrategySpecification;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class DatumArray extends SerializableBean {

    private static final long serialVersionUID = 3281986861557667658L;

    private List<Datum> datumList = new ArrayList<Datum> ();

    private static final TraversalStrategySpecification defaultTraversalStrategy
        = new TraversalStrategySpecification () {

        @Override
        public void execute(SerializableBean serializableBean, Collection<Consumer<SerializableBean>> visitors) {

            DatumArray datumArray = (DatumArray) serializableBean;

            if (datumArray.datumList != null)
                datumArray.datumList.forEach(datum -> { datum.accept(visitors); });

            visitors.forEach(visitor -> { visitor.accept(datumArray); } );
        }
    };

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {
        accept(defaultTraversalStrategy, visitors);
    }

    public void forEachDatum (Consumer<Datum> consumer) {
        datumList.forEach(consumer);
    }

    public List<Datum> getDatumList() {
        return datumList;
    }

    public void setDatumList(List<Datum> datumList) {
        this.datumList = datumList;
    }

    public DatumArray addDatum (Datum datum) {

        datumList.add(datum);

        return this;
    }

    public Datum getDatumAt (int index) {

//        if (datumList.size() < index)
//            throw new IndexOutOfBoundsException("The datumList.size (" + datumList.size() + ") is less than the index parameter value (" + index + "); datumList: " + datumList);

        return datumList.get(index);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((datumList == null) ? 0 : datumList.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        DatumArray other = (DatumArray) obj;
        if (datumList == null) {
            if (other.datumList != null)
                return false;
        } else if (!datumList.equals(other.datumList))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "DatumArray [datumList=" + datumList + "]";
    }
}
