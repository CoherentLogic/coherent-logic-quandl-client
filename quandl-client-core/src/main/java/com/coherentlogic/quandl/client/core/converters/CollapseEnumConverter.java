package com.coherentlogic.quandl.client.core.converters;

import com.coherentlogic.quandl.client.core.domain.timeseries.Collapse;
import com.thoughtworks.xstream.converters.enums.EnumSingleValueConverter;

import net.bytebuddy.asm.Advice.Return;

/**
 * @TODO {@link Return} null when nil=true
 *
 * <limit nil="true"/>
 * <transform nil="true"/>
 * <column-index nil="true"/>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class CollapseEnumConverter extends EnumSingleValueConverter {

    public CollapseEnumConverter(Class<? extends Enum> type) {
        super(type);
    }

    @Override
    public Object fromString(String value) {
        return (value == null || "".equals (value)) ? null : Collapse.valueOf(value);
    }
}
