package com.coherentlogic.quandl.client.core.converters.timeseries.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.exceptions.ConversionFailedException;
import com.coherentlogic.coherent.data.adapter.core.exceptions.MethodNotSupportedException;
import com.coherentlogic.quandl.client.core.domain.timeseries.ColumnNames;
import com.coherentlogic.quandl.client.core.domain.timeseries.data.Data;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 * <quandl-response>
<dataset-data>
<limit type="integer">5</limit>
<transform nil="true"/>
<column-index type="integer">1</column-index>
<column-names type="array">
<column-name>Date</column-name>
<column-name>Open</column-name>
</column-names>
<start-date type="date">2016-01-01</start-date>
<end-date type="date">2018-03-27</end-date>
<frequency>daily</frequency>
<data type="array">
<datum type="array">
<datum type="date">2018-03-31</datum>
<datum type="float">156.31</datum>
</datum>
<datum type="array">
<datum type="date">2018-02-28</datum>
<datum type="float">182.3</datum>
</datum>
<datum type="array">
<datum type="date">2018-01-31</datum>
<datum type="float">188.37</datum>
</datum>
<datum type="array">
<datum type="date">2017-12-31</datum>
<datum type="float">178.0</datum>
</datum>
<datum type="array">
<datum type="date">2017-11-30</datum>
<datum type="float">176.85</datum>
</datum>
</data>
<collapse>monthly</collapse>
<order nil="true"/>
</dataset-data>
</quandl-response>

 * @TODO: data, above, is marked as an array -- May need to change the result accordingly.
 */
public class DataConverter implements Converter {

    private static final Logger log = LoggerFactory.getLogger(DataConverter.class);

    static final String TYPE = "type", ARRAY = "array";

    @Override
    public boolean canConvert(Class clazz) {
        return Data.class.equals(clazz);
    }

    @Override
    public void marshal(Object arg0, HierarchicalStreamWriter arg1, MarshallingContext arg2) {
        throw new MethodNotSupportedException("The marshal method is not supported.");
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        Data result = new Data ();

        // @see the ColumnNames converter as this is added to the context there.
        ColumnNames columnNames = (ColumnNames) context.get(ColumnNames.COLUMN_NAMES);

        if (columnNames == null)
            throw new NullPointerException("Null columnNames reference retrieved from the context.");

        log.info ("columnNames: " + columnNames);

        processDatumArray (columnNames, result, reader, context);

        return result;
    }

    /**
     * <datum type="array">
<datum type="date">2018-03-31</datum>
<datum type="float">156.31</datum>
</datum>
     * @param columnNames
     * @param data
     * @param reader
     * @param context
     */
    private void processDatumArray (
        ColumnNames columnNames,
        Data data,
        HierarchicalStreamReader reader,
        UnmarshallingContext context
    ) {
        while (reader.hasMoreChildren()) {

            reader.moveDown();

            String type = reader.getAttribute(TYPE);

            log.debug ("type: " + type);

            if (ARRAY.equals(type))
                processDatumArrayEntries (columnNames, data, reader, context);
            else
                throw new ConversionFailedException ("Expected the type to be 'array' however " + type
                    + " was present instead.");

            reader.moveUp ();
        }
    }

    private void processDatumArrayEntries (
        ColumnNames columnNames,
        Data data,
        HierarchicalStreamReader reader,
        UnmarshallingContext context
    ) {
        int ctr = 0;

        while (reader.hasMoreChildren()) {

            reader.moveDown();

            String columnName = columnNames.getColumnNameAtIndex(ctr++);

            String type = reader.getAttribute(TYPE);

            String value = reader.getValue();

            log.debug("Creating a new datum for the columnName: " + columnName + " with type: " + type + ", and value: "
                + value);

            data.newDatum(columnName, type).addValue(value);

            reader.moveUp();
        }
    }
}
