package com.coherentlogic.quandl.client.core.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class ParseRuntimeException extends NestedRuntimeException {

    private static final long serialVersionUID = 3608810565672748523L;

    public ParseRuntimeException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
