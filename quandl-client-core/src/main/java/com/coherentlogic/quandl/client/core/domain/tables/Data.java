package com.coherentlogic.quandl.client.core.domain.tables;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.coherent.data.model.core.strategies.TraversalStrategySpecification;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class Data extends SerializableBean {

    private static final long serialVersionUID = -533521227059916831L;

    public static final String DATA = "data";

    private List<DatumArray> datumArrayList; // datumArray = one row

    private String type;

    private static final TraversalStrategySpecification defaultTraversalStrategy
        = new TraversalStrategySpecification () {

        @Override
        public void execute(
            SerializableBean serializableBean,
            Collection<Consumer<SerializableBean>> visitors
        ) {

            Data data = (Data) serializableBean;

            if (data.datumArrayList != null)
                data.datumArrayList.forEach(datumArray -> { datumArray.accept(visitors); });

            visitors.forEach(visitor -> { visitor.accept(data); } );
        }
    };

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {
        accept(defaultTraversalStrategy, visitors);
    }

    public void forEachDatumArray (Consumer<DatumArray> consumer) {
        datumArrayList.forEach(consumer);
    }

    public List<DatumArray> getDatumArrayList() {
        return datumArrayList;
    }

    public void setDatumArrayList(List<DatumArray> datumArrayList) {
        this.datumArrayList = datumArrayList;
    }

    public DatumArray newDatumArray () {

        DatumArray result = new DatumArray ();

        if (datumArrayList == null) {
            datumArrayList = new ArrayList<DatumArray> ();
        }

        datumArrayList.add(result);

        return result;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((datumArrayList == null) ? 0 : datumArrayList.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Data other = (Data) obj;
        if (datumArrayList == null) {
            if (other.datumArrayList != null)
                return false;
        } else if (!datumArrayList.equals(other.datumArrayList))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Data [datumArrayList=" + datumArrayList + ", type=" + type + "]";
    }
}
