package com.coherentlogic.quandl.client.core.domain.timeseries;

import java.util.List;
import java.util.function.Consumer;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=ColumnNames.COLUMN_NAMES)
@XStreamAlias(ColumnNames.COLUMN_NAMES_ALIAS)
@Visitable
public class ColumnNames extends SerializableBean {

    private static final long serialVersionUID = 7883672223604991936L;

    public static final String
        COLUMN_NAMES = "columnNames",
        COLUMN_NAMES_ALIAS = "column-names",
        COLUMN_NAME_ALIAS = "column-name";

    @XStreamImplicit
    @XStreamAlias(ColumnNames.COLUMN_NAME_ALIAS)
    private List<String> columnNames;

    public void forEachColumnName (Consumer<? super String> consumer) {
        columnNames.forEach(consumer);
    }

    public List<String> getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(@Changeable(COLUMN_NAMES) List<String> columnNames) {
        this.columnNames = columnNames;
    }

    public String getColumnNameAtIndex (int index) {

        int size = columnNames.size();

        if (size < index)
            throw new IndexOutOfBoundsException("The index (" + index + ") is greater than the size (" +
                size + ") of the columnNames list.");

        String result = columnNames.get(index);

        return result;
    }

    public int size () {
        return columnNames.size();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((columnNames == null) ? 0 : columnNames.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ColumnNames other = (ColumnNames) obj;
        if (columnNames == null) {
            if (other.columnNames != null)
                return false;
        } else if (!columnNames.equals(other.columnNames))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ColumnNames [columnNames=" + columnNames + "]";
    }
}
