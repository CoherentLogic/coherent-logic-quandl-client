package com.coherentlogic.quandl.client.core.domain.timeseries;

/**
 * https://docs.quandl.com/docs/parameters-2
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public enum Collapse {

    none,
    daily,
    weekly,
    monthly,
    quarterly,
    annual
}
