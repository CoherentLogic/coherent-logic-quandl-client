package com.coherentlogic.quandl.client.core.oxm.xstream;

import com.coherentlogic.quandl.client.core.domain.timeseries.data.Data;
import com.coherentlogic.quandl.client.core.domain.timeseries.data.DatasetData;
import com.coherentlogic.quandl.client.core.domain.timeseries.data.Datum;
import com.coherentlogic.quandl.client.core.domain.timeseries.metadata.Dataset;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.security.NoTypePermission;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class XStreamMarshaller extends org.springframework.oxm.xstream.XStreamMarshaller {

    /**
     * @see https://groups.google.com/forum/#!topic/xstream-user/wiKfdJPL8aY
     * 
     * @throws com.thoughtworks.xstream.security.ForbiddenClassException
     */
    @Override
    protected void customizeXStream(XStream xstream) {

        super.customizeXStream(xstream);

        /* Required otherwise the DatasetDataConverter will fail as it will be unable to unmarshall the data field.
         */
        xstream.ignoreUnknownElements();

        xstream.addPermission(NoTypePermission.NONE);

        xstream.allowTypes(
            new Class[] {
                Dataset.class,
                DatasetData.class,
                Data.class,
                Datum.class,
                com.coherentlogic.quandl.client.core.domain.timeseries.metadata.QuandlResponse.class,
                com.coherentlogic.quandl.client.core.domain.timeseries.data.QuandlResponse.class,
                com.coherentlogic.quandl.client.core.domain.tables.QuandlResponse.class,
                com.coherentlogic.quandl.client.core.domain.tables.Columns.class,
                com.coherentlogic.quandl.client.core.domain.tables.Column.class,
                com.coherentlogic.quandl.client.core.domain.tables.DataTable.class
            }
        );
    }
}
