package com.coherentlogic.quandl.client.core.domain.tables;

import java.util.Collection;
import java.util.function.Consumer;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.coherent.data.model.core.strategies.TraversalStrategySpecification;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @see <a href="https://docs.quandl.com/docs/in-depth-usage#section-get-time-series-metadata">Get time-series metadata</a>
 * @see <a href="https://www.quandl.com/api/v3/datasets/WIKI/FB/metadata.xml">Metadata</a>
 */
@Entity
@Table(name = QuandlResponse.TIME_SERIES_METADATA_TABLE_NAME)
@XStreamAlias(QuandlResponse.TIME_SERIES_METADATA_QUANDL_RESPONSE_ALIAS)
@Visitable
public class QuandlResponse extends SerializableBean {

    private static final long serialVersionUID = 4521945783542773807L;

    static final String TIME_SERIES_METADATA_TABLE_NAME = "time_series_metadata_quandl_response";

    static final String TIME_SERIES_METADATA_QUANDL_RESPONSE_ALIAS = "quandl-response";

    @XStreamAlias(DataTable.DATA_TABLE_ALIAS)
    private DataTable dataTable;

    @XStreamAlias(Meta.META_ALIAS)
    private Meta meta;

    private static final TraversalStrategySpecification defaultTraversalStrategy
        = new TraversalStrategySpecification () {

        @Override
        public void execute(
            SerializableBean serializableBean,
            Collection<Consumer<SerializableBean>> visitors
        ) {

            QuandlResponse quandlResponse = (QuandlResponse) serializableBean;

            if (quandlResponse.dataTable != null)
                quandlResponse.dataTable.accept(visitors);

            if (quandlResponse.meta != null)
                quandlResponse.meta.accept(visitors);

            visitors.forEach(visitor -> { visitor.accept(quandlResponse); } );
        }
    };

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {
        accept(defaultTraversalStrategy, visitors);
    }

    public DataTable getDataTable() {
        return dataTable;
    }

    public void setDataTable(DataTable dataTable) {
        this.dataTable = dataTable;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((dataTable == null) ? 0 : dataTable.hashCode());
        result = prime * result + ((meta == null) ? 0 : meta.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        QuandlResponse other = (QuandlResponse) obj;
        if (dataTable == null) {
            if (other.dataTable != null)
                return false;
        } else if (!dataTable.equals(other.dataTable))
            return false;
        if (meta == null) {
            if (other.meta != null)
                return false;
        } else if (!meta.equals(other.meta))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "QuandlResponse [dataTable=" + dataTable + ", meta=" + meta + "]";
    }
}
/*
<quandl-response>
<datatable>
<data type="array">
<datum type="array">
<datum type="date">2010-12-31</datum>
<datum type="float">11.444623</datum>
</datum>
<datum type="array">
<datum type="date">2011-12-31</datum>
<datum type="float">10.420364</datum>
</datum>
<datum type="array">
<datum type="date">2012-12-31</datum>
<datum type="float">8.109622</datum>
</datum>
<datum type="array">
<datum type="date">2013-12-31</datum>
<datum type="float">3.423688</datum>
</datum>
<datum type="array">
<datum type="date">2014-12-31</datum>
<datum type="float">3.442269</datum>
</datum>
<datum type="array">
<datum type="date">2015-12-31</datum>
<datum type="float">3.404856</datum>
</datum>
<datum type="array">
<datum type="date">2010-12-31</datum>
<datum type="float">11.444623</datum>
</datum>
<datum type="array">
<datum type="date">2011-12-31</datum>
<datum type="float">10.420364</datum>
</datum>
<datum type="array">
<datum type="date">2012-12-31</datum>
<datum type="float">8.109622</datum>
</datum>
<datum type="array">
<datum type="date">2013-12-31</datum>
<datum type="float">3.423688</datum>
</datum>
<datum type="array">
<datum type="date">2014-12-31</datum>
<datum type="float">3.442269</datum>
</datum>
<datum type="array">
<datum type="date">2015-12-31</datum>
<datum type="float">3.404856</datum>
</datum>
</data>
<columns type="array">
<column>
<name>reportdate</name>
<type>Date</type>
</column>
<column>
<name>amount</name>
<type>BigDecimal(36,14)</type>
</column>
</columns>
</datatable>
<meta>
<next-cursor-id nil="true"/>
</meta>
</quandl-response>
*/