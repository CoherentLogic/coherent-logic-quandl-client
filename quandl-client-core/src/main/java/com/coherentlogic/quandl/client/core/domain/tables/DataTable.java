package com.coherentlogic.quandl.client.core.domain.tables;

import java.util.Collection;
import java.util.function.Consumer;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.coherent.data.model.core.strategies.TraversalStrategySpecification;
import com.coherentlogic.quandl.client.core.converters.table.DataConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@XStreamAlias(DataTable.DATA_TABLE_ALIAS)
public class DataTable extends SerializableBean {

    private static final long serialVersionUID = -5556848831267337432L;

    public static final String DATA_TABLE_ALIAS = "datatable";

    @XStreamAlias(Data.DATA)
    @XStreamConverter(DataConverter.class)
    private Data data;

    @XStreamAlias(Columns.COLUMNS)
    private Columns columns = null;

    private static final TraversalStrategySpecification defaultTraversalStrategy
        = new TraversalStrategySpecification () {

        @Override
        public void execute(
            SerializableBean serializableBean,
            Collection<Consumer<SerializableBean>> visitors
        ) {

            DataTable dataTable = (DataTable) serializableBean;

            if (dataTable.data != null)
                dataTable.data.accept(visitors);

            if (dataTable.columns != null)
                dataTable.columns.accept(visitors);

            visitors.forEach(visitor -> { visitor.accept(dataTable); } );
        }
    };

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {
        accept(defaultTraversalStrategy, visitors);
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Columns getColumns() {
        return columns;
    }

    public void setColumns(Columns columns) {
        this.columns = columns;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((columns == null) ? 0 : columns.hashCode());
        result = prime * result + ((data == null) ? 0 : data.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        DataTable other = (DataTable) obj;
        if (columns == null) {
            if (other.columns != null)
                return false;
        } else if (!columns.equals(other.columns))
            return false;
        if (data == null) {
            if (other.data != null)
                return false;
        } else if (!data.equals(other.data))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "DataTable [columns=" + columns + ", data=" + data + "]";
    }
}
/*
https://www.quandl.com/api/v3/datatables/MER/F1.xml?&mapcode=-5370&compnumber=39102&reporttype=A&qopts.columns=reportdate,amount&api_key=[redacted]

<quandl-response>
<datatable>
<data type="array">
<datum type="array">
<datum type="date">2010-12-31</datum>
<datum type="float">11.444623</datum>
</datum>
<datum type="array">
<datum type="date">2011-12-31</datum>
<datum type="float">10.420364</datum>
</datum>
<datum type="array">
<datum type="date">2012-12-31</datum>
<datum type="float">8.109622</datum>
</datum>
<datum type="array">
<datum type="date">2013-12-31</datum>
<datum type="float">3.423688</datum>
</datum>
<datum type="array">
<datum type="date">2014-12-31</datum>
<datum type="float">3.442269</datum>
</datum>
<datum type="array">
<datum type="date">2015-12-31</datum>
<datum type="float">3.404856</datum>
</datum>
<datum type="array">
<datum type="date">2010-12-31</datum>
<datum type="float">11.444623</datum>
</datum>
<datum type="array">
<datum type="date">2011-12-31</datum>
<datum type="float">10.420364</datum>
</datum>
<datum type="array">
<datum type="date">2012-12-31</datum>
<datum type="float">8.109622</datum>
</datum>
<datum type="array">
<datum type="date">2013-12-31</datum>
<datum type="float">3.423688</datum>
</datum>
<datum type="array">
<datum type="date">2014-12-31</datum>
<datum type="float">3.442269</datum>
</datum>
<datum type="array">
<datum type="date">2015-12-31</datum>
<datum type="float">3.404856</datum>
</datum>
</data>
<columns type="array">
<column>
<name>reportdate</name>
<type>Date</type>
</column>
<column>
<name>amount</name>
<type>BigDecimal(36,14)</type>
</column>
</columns>
</datatable>
<meta>
<next-cursor-id nil="true"/>
</meta>
</quandl-response>
*/
