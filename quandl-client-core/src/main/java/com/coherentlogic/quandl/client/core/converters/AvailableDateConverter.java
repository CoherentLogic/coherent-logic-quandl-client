package com.coherentlogic.quandl.client.core.converters;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.exceptions.MethodNotSupportedException;
import com.coherentlogic.quandl.client.core.exceptions.ParseRuntimeException;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class AvailableDateConverter implements Converter {

    private static final Logger log = LoggerFactory.getLogger(AvailableDateConverter.class);

    static final String DATE_FORMAT = "yyyy-MM-dd";

    private static final DateFormat dateFormat = new SimpleDateFormat (DATE_FORMAT);

    @Override
    public boolean canConvert(Class type) {
        return Date.class.equals(type);
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        throw new MethodNotSupportedException("The marshal method is not supported.");
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        String value = reader.getValue();

        //log.info
        System.out.println ("Attempting to convert the value: " + value + " using the format: " + DATE_FORMAT);

        try {

            Date result = dateFormat.parse(value);

            System.out.println ("result: " + result);

            return result;

        } catch (ParseException parseException) {
            throw new ParseRuntimeException("Unable to parse the date " + value + " using the format " +
                DATE_FORMAT, parseException);
        }
    }
}
