package com.coherentlogic.quandl.client.core.domain.timeseries.data;

import java.util.Collection;
import java.util.function.Consumer;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.coherent.data.model.core.strategies.TraversalStrategySpecification;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=QuandlResponse.QUANDL_RESPONSE)
@XStreamAlias(QuandlResponse.QUANDL_RESPONSE)
@Visitable
public class QuandlResponse extends SerializableBean {

    private static final long serialVersionUID = -1834349105355153921L;

    public static final String QUANDL_RESPONSE = "quandl-response";

    @XStreamAlias(DatasetData.DATASET_DATA_ALIAS)
    @Visitable
    private DatasetData datasetData;

    private static final TraversalStrategySpecification defaultTraversalStrategy
        = new TraversalStrategySpecification () {

        @Override
        public void execute(
            SerializableBean serializableBean,
            Collection<Consumer<SerializableBean>> visitors
        ) {
            QuandlResponse quandlResponse = (QuandlResponse) serializableBean;

            if (quandlResponse.datasetData != null)
                quandlResponse.datasetData.accept(visitors);

            visitors.forEach(visitor -> { visitor.accept(quandlResponse); });
        }
    };

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {
        accept(defaultTraversalStrategy, visitors);
    }

    public DatasetData getDatasetData() {
        return datasetData;
    }

    public void setDatasetData(@Changeable (DatasetData.DATASET_DATA) DatasetData datasetData) {
        this.datasetData = datasetData;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((datasetData == null) ? 0 : datasetData.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        QuandlResponse other = (QuandlResponse) obj;
        if (datasetData == null) {
            if (other.datasetData != null)
                return false;
        } else if (!datasetData.equals(other.datasetData))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "QuandlResponse [datasetData=" + datasetData + "]";
    }
}
