package com.coherentlogic.quandl.client.core.converters;

import com.coherentlogic.quandl.client.core.domain.timeseries.Transform;
import com.thoughtworks.xstream.converters.enums.EnumSingleValueConverter;

/**
 * Converter for transform enumerations, which have a nil flag.
 *
 * <limit nil="true"/>
 * <transform nil="true"/>
 * <column-index nil="true"/>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class TransformEnumConverter extends EnumSingleValueConverter {

    public TransformEnumConverter(Class<? extends Enum> type) {
        super(type);
    }

    @Override
    public Object fromString(String value) {
        return (value == null || "".equals(value)) ? null : Transform.valueOf(value);
    }
}
