package com.coherentlogic.quandl.client.core.converters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.exceptions.MethodNotSupportedException;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * <limit nil="true"/>
 * <transform nil="true"/>
 * <column-index nil="true"/>
 * @author Thomas P. Fuller
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class NillableIntegerValueReflectionConverter implements Converter {

    private static final Logger log = LoggerFactory.getLogger(NillableIntegerValueReflectionConverter.class);

    static final String NIL = "nil";

    @Override
    public boolean canConvert(Class type) {
        return Integer.class.equals(type);
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        throw new MethodNotSupportedException("The marshal method is not supported.");
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        String nilAttribute = reader.getAttribute(NIL);

        Object result = null;

        if ( nilAttribute == null || !"true".equals(nilAttribute) ) {

            String value = reader.getValue();

            log.debug ("value: " + value);

            result = Integer.valueOf(value);
        }

        log.debug ("nilAttribute: " + nilAttribute + ", result: " + result);

        return result;
    }
}
