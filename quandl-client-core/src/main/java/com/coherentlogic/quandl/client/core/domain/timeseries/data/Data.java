package com.coherentlogic.quandl.client.core.domain.timeseries.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.coherent.data.model.core.strategies.TraversalStrategySpecification;
import com.coherentlogic.quandl.client.core.exceptions.NonuniformColumnsException;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @TODO Shared in a few placed but should probably be in the domain.timeseries.data package.
 * @see DatasetData
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=Data.DATA)
@XStreamAlias(Data.DATA)
@Visitable
public class Data extends SerializableBean {

    private static final long serialVersionUID = -6650757096382852687L;

    public static final String DATA = "data", TYPE = "type", VALUES = "values";

    /**
     * Name : Type : Values
     */
    private Map<String, Datum> datumMap = new HashMap<String, Datum> ();

    private static final TraversalStrategySpecification defaultTraversalStrategy
        = new TraversalStrategySpecification () {

        @Override
        public void execute(
            SerializableBean serializableBean,
            Collection<Consumer<SerializableBean>> visitors
        ) {
            Data data = (Data) serializableBean;

            data
                .datumMap
                .entrySet()
                .forEach(
                    entry -> {

                        Datum datum = entry.getValue();

                        datum.accept (visitors); 
                    }
                );

            visitors.forEach(visitor -> { visitor.accept(data); });
        }
    };

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {
        accept(defaultTraversalStrategy, visitors);
    }

    public Map<String, Datum> getDatumMap() {
        return datumMap;
    }

    /**
     * Delegate method to datumMap.forEach.
     *
     * @param action will be executed for each name and datum entry.
     */
    public void forEachDatum (BiConsumer<? super String, ? super Datum> action) {
        datumMap.forEach(action);
    }

    public void setDatumList(Map<String, Datum> datumMap) {
        this.datumMap = datumMap;
    }

    public Datum getDatumByColumnName (String columnName) {

        Datum result = datumMap.get(columnName);

        // If the result is null a new datum will be created (see the #newDatum method below).

        return result;
    }

    public Datum newDatum (String columnName, String type) {

        Datum result = getDatumByColumnName (columnName);

        if (result == null) {

            result = new Datum ();

            result.setType(type);

            datumMap.put(columnName, result);
        }

        return result;
    }

    public Map<String, Integer> getDatumSize () {

        Map<String, Integer> result = new HashMap<String, Integer> ();

        datumMap.entrySet().forEach(entry -> {

            String columnName = entry.getKey();

            int size = entry.getValue().size();

            result.put(columnName, size);
        });

        return result;
    }

    class DatumSizeCounter implements Consumer<Integer> {

        private Integer result = null;

        private Map<String, Integer> datumSizeMap;

        public DatumSizeCounter(Map<String, Integer> datumSizeMap) {
            this.datumSizeMap = datumSizeMap;
        }

        @Override
        public void accept(Integer value) {

            datumSizeMap.values().forEach(
                size -> {

                    if (result == null)
                        result = size;
                    else {
                        if (result != null && !result.equals(size))
                            throw new NonuniformColumnsException ("The columns are not uniform (result: " + result +
                                ", datumSizeMap: " + datumSizeMap + ").");
                        else
                            result = size;
                    }
                }
            );
        }

        public Integer getResult() {
            return result;
        }
    }

    /**
     * Returns a single size which reflects the size of all rows in all datum instances. If the size differs at all an
     * exception is thrown.
     */
    public Integer getUniformDatumSize () {

        Map<String, Integer> datumSizeMap = getDatumSize ();

        DatumSizeCounter datumSizeCounter = new DatumSizeCounter (datumSizeMap);

        datumSizeMap.values().forEach(datumSizeCounter);

        return datumSizeCounter.getResult();
    }

    public void forEachRow (Consumer<Row> consumer) {

        final AtomicInteger rowCtr = new AtomicInteger (0);

        Row row = new Row ();

        Set<Entry<String, Datum>> columns = datumMap.entrySet();

        final AtomicInteger uniformSize = new AtomicInteger (getUniformDatumSize());

        while (rowCtr.get() < uniformSize.get()) {

            columns.forEach(entry -> {

                String columnName = entry.getKey();

                Datum datum = entry.getValue();

                String columnValue = datum.getValueAt(rowCtr.get());

                row.putColumn(columnName, columnValue);

            });

            consumer.accept(row);

            rowCtr.incrementAndGet();
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((datumMap == null) ? 0 : datumMap.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Data other = (Data) obj;
        if (datumMap == null) {
            if (other.datumMap != null)
                return false;
        } else if (!datumMap.equals(other.datumMap))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Data [datumMap=" + datumMap + "]";
    }
}
