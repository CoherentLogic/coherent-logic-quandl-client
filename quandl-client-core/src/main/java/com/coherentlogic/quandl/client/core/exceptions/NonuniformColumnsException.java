package com.coherentlogic.quandl.client.core.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class NonuniformColumnsException extends NestedRuntimeException {

    private static final long serialVersionUID = 2428085592697453448L;

    public NonuniformColumnsException(String msg) {
        super(msg);
    }
}
