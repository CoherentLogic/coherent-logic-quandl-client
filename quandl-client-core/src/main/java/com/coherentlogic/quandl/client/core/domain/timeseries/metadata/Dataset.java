package com.coherentlogic.quandl.client.core.domain.timeseries.metadata;

import java.util.Collection;
import java.util.Date;
import java.util.function.Consumer;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.domain.IdentitySpecification;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.coherent.data.model.core.strategies.TraversalStrategySpecification;
import com.coherentlogic.quandl.client.core.converters.AvailableDateConverter;
import com.coherentlogic.quandl.client.core.converters.timeseries.ColumnNamesConverter;
import com.coherentlogic.quandl.client.core.domain.timeseries.ColumnNames;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@XStreamAlias(Dataset.DATASET)
public class Dataset extends SerializableBean implements IdentitySpecification<Integer> {

    private static final long serialVersionUID = -2187298499577992375L;

    public static final String DATASET = "dataset";

    static final String ID = "id";

    @XStreamAlias(ID)
    private Integer id;

    @XStreamAlias(NAME)
    private String name;

    @XStreamAlias(DESCRIPTION)
    private String description;

    @XStreamAlias(REFRESHED_AT_ALIAS)
    private Date refreshedAt;

    @XStreamAlias(NEWEST_AVAILABLE_DATE_ALIAS)
    @XStreamConverter(AvailableDateConverter.class)
    private Date newestAvailableDate;

    @XStreamAlias(OLDEST_AVAILABLE_DATE_ALIAS)
    @XStreamConverter(AvailableDateConverter.class)
    private Date oldestAvailableDate;

    @XStreamAlias(ColumnNames.COLUMN_NAMES_ALIAS)
    @XStreamConverter(ColumnNamesConverter.class)
    private ColumnNames columnNames;

    @XStreamAlias(FREQUENCY)
    private Frequency frequency;

    /**
     * Check that this is limited to "Time Series" and (probably) "Table".
     */
    @XStreamAlias(TYPE)
    private String type;

    @XStreamAlias(PREMIUM)
    private Boolean premium;

    static final String DATABASE_ID = "databaseId", DATABASE_ID_ALIAS = "database-id";

    @XStreamAlias(DATABASE_ID_ALIAS)
    private Integer databaseId;

    private static final TraversalStrategySpecification defaultTraversalStrategy
        = new TraversalStrategySpecification () {

        @Override
        public void execute(
            SerializableBean serializableBean, Collection<Consumer<SerializableBean>> visitors) {

            Dataset dataset = (Dataset) serializableBean;

            if (dataset.columnNames != null)
                dataset.columnNames.accept(visitors);

            visitors.forEach(visitor -> { visitor.accept(dataset); } );
        }
    };

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {
        accept(defaultTraversalStrategy, visitors);
    }

    public String getName() {
        return name;
    }

    static final String NAME = "name";

    public void setName(@Changeable (NAME) String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    static final String DESCRIPTION = "description";

    public void setDescription(@Changeable (DESCRIPTION) String description) {
        this.description = description;
    }

    public Date getRefreshedAt() {
        return refreshedAt;
    }

    static final String REFRESHED_AT = "refreshedAt", REFRESHED_AT_ALIAS = "refreshed-at";

    public void setRefreshedAt(@Changeable (REFRESHED_AT) Date refreshedAt) {
        this.refreshedAt = refreshedAt;
    }

    public Date getNewestAvailableDate() {
        return newestAvailableDate;
    }

    static final String NEWEST_AVAILABLE_DATE = "newestAvailableDate",
        NEWEST_AVAILABLE_DATE_ALIAS = "newest-available-date";

    public void setNewestAvailableDate(@Changeable (NEWEST_AVAILABLE_DATE) Date newestAvailableDate) {
        this.newestAvailableDate = newestAvailableDate;
    }

    public Date getOldestAvailableDate() {
        return oldestAvailableDate;
    }

    static final String OLDEST_AVAILABLE_DATE = "oldestAvailableDate",
        OLDEST_AVAILABLE_DATE_ALIAS = "oldest-available-date";

    public void setOldestAvailableDate(@Changeable (OLDEST_AVAILABLE_DATE) Date oldestAvailableDate) {
        this.oldestAvailableDate = oldestAvailableDate;
    }

    public ColumnNames getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(@Changeable (ColumnNames.COLUMN_NAMES) ColumnNames columnNames) {
        this.columnNames = columnNames;
    }

    public Frequency getFrequency() {
        return frequency;
    }

    static final String FREQUENCY = "frequency";

    public void setFrequency(@Changeable (FREQUENCY) Frequency frequency) {
        this.frequency = frequency;
    }

    public String getType() {
        return type;
    }

    static final String TYPE = "type";

    public void setType(@Changeable (TYPE) String type) {
        this.type = type;
    }

    public Boolean getPremium() {
        return premium;
    }

    static final String PREMIUM = "premium";

    public void setPremium(@Changeable (PREMIUM) Boolean premium) {
        this.premium = premium;
    }

    public Integer getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(@Changeable (DATABASE_ID) Integer databaseId) {
        this.databaseId = databaseId;
    }

    @Override
    public void setId(@Changeable (ID) Integer id) {
        this.id = id;
    }

    @Override
    public Integer getId() {
        return id;
    }

    static final String DATASET_CODE = "datasetCode", DATASET_CODE_ALIAS = "dataset-code";

    @XStreamAlias(DATASET_CODE_ALIAS)
    private String datasetCode;

    public String getDatasetCode() {
        return datasetCode;
    }

    public void setDatasetCode(@Changeable (DATASET_CODE) String datasetCode) {
        this.datasetCode = datasetCode;
    }

    static final String DATABASE_CODE = "databaseCode", DATABASE_CODE_ALIAS = "database-code";

    @XStreamAlias(DATABASE_CODE_ALIAS)
    private String databaseCode;

    public String getDatabaseCode() {
        return databaseCode;
    }

    public void setDatabaseCode(@Changeable (DATABASE_CODE) String databaseCode) {
        this.databaseCode = databaseCode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((columnNames == null) ? 0 : columnNames.hashCode());
        result = prime * result + ((databaseCode == null) ? 0 : databaseCode.hashCode());
        result = prime * result + ((databaseId == null) ? 0 : databaseId.hashCode());
        result = prime * result + ((datasetCode == null) ? 0 : datasetCode.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((frequency == null) ? 0 : frequency.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((newestAvailableDate == null) ? 0 : newestAvailableDate.hashCode());
        result = prime * result + ((oldestAvailableDate == null) ? 0 : oldestAvailableDate.hashCode());
        result = prime * result + ((premium == null) ? 0 : premium.hashCode());
        result = prime * result + ((refreshedAt == null) ? 0 : refreshedAt.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Dataset other = (Dataset) obj;
        if (columnNames == null) {
            if (other.columnNames != null)
                return false;
        } else if (!columnNames.equals(other.columnNames))
            return false;
        if (databaseCode == null) {
            if (other.databaseCode != null)
                return false;
        } else if (!databaseCode.equals(other.databaseCode))
            return false;
        if (databaseId == null) {
            if (other.databaseId != null)
                return false;
        } else if (!databaseId.equals(other.databaseId))
            return false;
        if (datasetCode == null) {
            if (other.datasetCode != null)
                return false;
        } else if (!datasetCode.equals(other.datasetCode))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (frequency != other.frequency)
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (newestAvailableDate == null) {
            if (other.newestAvailableDate != null)
                return false;
        } else if (!newestAvailableDate.equals(other.newestAvailableDate))
            return false;
        if (oldestAvailableDate == null) {
            if (other.oldestAvailableDate != null)
                return false;
        } else if (!oldestAvailableDate.equals(other.oldestAvailableDate))
            return false;
        if (premium == null) {
            if (other.premium != null)
                return false;
        } else if (!premium.equals(other.premium))
            return false;
        if (refreshedAt == null) {
            if (other.refreshedAt != null)
                return false;
        } else if (!refreshedAt.equals(other.refreshedAt))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Dataset [id=" + id + ", name=" + name + ", description=" + description + ", refreshedAt=" + refreshedAt
            + ", newestAvailableDate=" + newestAvailableDate + ", oldestAvailableDate=" + oldestAvailableDate
            + ", columnNames=" + columnNames + ", frequency=" + frequency + ", type=" + type + ", premium="
            + premium + ", databaseId=" + databaseId + ", datasetCode=" + datasetCode + ", databaseCode="
            + databaseCode + "]";
    }
}
