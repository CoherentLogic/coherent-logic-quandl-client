package com.coherentlogic.quandl.client.core.converters.table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.exceptions.ConversionFailedException;
import com.coherentlogic.coherent.data.adapter.core.exceptions.MethodNotSupportedException;
import com.coherentlogic.quandl.client.core.domain.tables.Data;
import com.coherentlogic.quandl.client.core.domain.tables.Datum;
import com.coherentlogic.quandl.client.core.domain.tables.DatumArray;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class DataConverter implements Converter {

    private static final Logger log = LoggerFactory.getLogger(DataConverter.class);

    static final String TYPE = "type", ARRAY = "array";

    @Override
    public boolean canConvert(Class clazz) {
        return Data.class.equals(clazz);
    }

    @Override
    public void marshal(Object arg0, HierarchicalStreamWriter writer, MarshallingContext context) {
        throw new MethodNotSupportedException("The marshal method is not supported.");
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        Data result = new Data ();

        String type = reader.getAttribute(TYPE);

        result.setType(type);

        processDatumArray (result, reader, context);

        return result;
    }

    /**
     * <datum type="array">
     *   <datum type="date">2018-03-31</datum>
     *   <datum type="float">156.31</datum>
     * </datum>
     */
    private void processDatumArray (
        Data data,
        HierarchicalStreamReader reader,
        UnmarshallingContext context
    ) {
        while (reader.hasMoreChildren()) {

            reader.moveDown();

            String type = reader.getAttribute(TYPE);

            if (ARRAY.equals(type))
                processDatumArrayEntries (data, reader, context);
            else
                throw new ConversionFailedException ("Expected the type to be 'array' however " + type
                    + " was present instead.");

            reader.moveUp ();
        }
    }

    private void processDatumArrayEntries (
        Data data,
        HierarchicalStreamReader reader,
        UnmarshallingContext context
    ) {

        DatumArray datumArray = data.newDatumArray();

        while (reader.hasMoreChildren()) {

            reader.moveDown();

            String type = reader.getAttribute(TYPE);

            String value = reader.getValue();

            Datum datum = new Datum ();

            datum.setType(type);
            datum.setValue(value);

            datumArray.addDatum(datum);

            reader.moveUp();
        }
    }
}
