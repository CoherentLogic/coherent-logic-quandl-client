 package com.coherentlogic.quandl.client.core.domain.timeseries.data;

import java.util.Collection;
import java.util.Date;
import java.util.function.Consumer;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.coherent.data.model.core.strategies.TraversalStrategySpecification;
import com.coherentlogic.quandl.client.core.converters.CollapseEnumConverter;
import com.coherentlogic.quandl.client.core.converters.NillableIntegerValueReflectionConverter;
import com.coherentlogic.quandl.client.core.converters.TransformEnumConverter;
import com.coherentlogic.quandl.client.core.converters.timeseries.ColumnNamesConverter;
import com.coherentlogic.quandl.client.core.converters.timeseries.data.DataConverter;
import com.coherentlogic.quandl.client.core.converters.timeseries.data.FrequencyEnumConverter;
import com.coherentlogic.quandl.client.core.converters.timeseries.data.OrderEnumConverter;
import com.coherentlogic.quandl.client.core.domain.timeseries.Collapse;
import com.coherentlogic.quandl.client.core.domain.timeseries.ColumnNames;
import com.coherentlogic.quandl.client.core.domain.timeseries.Order;
import com.coherentlogic.quandl.client.core.domain.timeseries.Transform;
import com.coherentlogic.quandl.client.core.domain.timeseries.metadata.Frequency;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 * @TODO Shared in a few placed but should probably be in the domain.timeseries.data package.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name = DatasetData.DATASET_DATA)
@XStreamAlias(DatasetData.DATASET_DATA_ALIAS)
@Visitable
public class DatasetData extends SerializableBean {

    private static final long serialVersionUID = -5080431685288594886L;

    public static final String
        DATASET_DATA_ALIAS = "dataset-data",
        DATASET_DATA = "DATASET_DATA",
        DATA_LIST = "dataList",
        DATA_MAP = "dataMap",
        DATA = "data",
        ID = "id",
        LIMIT = "limit",
        TRANSFORM = "transform",
        START_DATE_ALIAS = "start-date",
        START_DATE = "startDate",
        END_DATE_ALIAS = "end-date",
        END_DATE = "endDate",
        COLUMN_INDEX = "columnIndex",
        COLUMN_INDEX_ALIAS = "column-index",
        COLUMN_NAMES = "columnNames",
        COLLAPSE = "collapse",
        ORDER = "order",
        FREQUENCY = "frequency";

    @XStreamAlias(LIMIT)
    @XStreamConverter(NillableIntegerValueReflectionConverter.class)
    @Visitable
    private Integer limit;

    @XStreamAlias(TRANSFORM)
    @XStreamConverter(TransformEnumConverter.class)
    @Visitable
    private Transform transform;

    @XStreamAlias(ColumnNames.COLUMN_NAMES_ALIAS)
    @XStreamConverter(ColumnNamesConverter.class)
    @Visitable
    private ColumnNames columnNames;

    // See the application context file as the converter is defined there (not working here at the moment and
    // I don't want to spend more time on figuring out why.
    // @XStreamConverter(value=DateConverter.class, strings= {"yyyy-MM-dd"})
    @XStreamAlias(DatasetData.START_DATE_ALIAS)
    private Date startDate;

    @XStreamAlias(DatasetData.END_DATE_ALIAS)
    private Date endDate;

    @XStreamAlias(COLUMN_INDEX_ALIAS)
    @XStreamConverter(NillableIntegerValueReflectionConverter.class)
    @Visitable
    private Integer columnIndex;

    @XStreamAlias(COLLAPSE)
    @XStreamConverter(CollapseEnumConverter.class)
    @Visitable
    private Collapse collapse;

    /**
     * This should probably be an enum, see Collapse, however I don't see the values this can have at this time so I'm
     * setting this to type string for now.
     */
    @XStreamAlias(FREQUENCY)
    @XStreamConverter(FrequencyEnumConverter.class)
    @Visitable
    private Frequency frequency;

    // TODO: Change back to Order
    @XStreamAlias(ORDER)
    @XStreamConverter(OrderEnumConverter.class)
    @Visitable
    private Order order;

    /**
     * Map -> List
     *
     * M:M:List
     *
     * columnName : columnType : List<columnValue> columnValues
     *
     * M:List<Bean>
     * 
     * columnName : Data where data has [type and values]
     * 
     */
    @XStreamAlias(DATA)
    @XStreamConverter(DataConverter.class)
    private Data data;

    private static final TraversalStrategySpecification defaultTraversalStrategy
        = new TraversalStrategySpecification () {

        @Override
        public void execute(
            SerializableBean serializableBean,
            Collection<Consumer<SerializableBean>> visitors
        ) {
            DatasetData datasetData = (DatasetData) serializableBean;

            if (datasetData.columnNames != null)
                datasetData.columnNames.accept(visitors);

            if (datasetData.data != null)
                datasetData.data.accept(visitors);

            visitors.forEach(visitor -> { visitor.accept(datasetData); });
        }
    };

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {
        accept(defaultTraversalStrategy, visitors);
    }

    /**
     * @TODO Consider changing this so that limit is a bean as this has a type attribute.
     */
    public Integer getLimit() {
        return limit;
    }

    public void setLimit(@Changeable(LIMIT) Integer limit) {
        this.limit = limit;
    }

    /**
     * @TODO Consider changing this so that transform is a bean as this has a nil flag attribute.
     */
    public Transform getTransform() {
        return transform;
    }

    public void setTransform(@Changeable(TRANSFORM) Transform transform) {
        this.transform = transform;
    }

    /**
     * @TODO Consider changing this so that column index is a bean as this has a type attribute.
     */
    public Integer getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(@Changeable(COLUMN_INDEX) Integer columnIndex) {
        this.columnIndex = columnIndex;
    }

    public Collapse getCollapse() {
        return collapse;
    }

    public void setCollapse(@Changeable(COLLAPSE) Collapse collapse) {
        this.collapse = collapse;
    }

    public ColumnNames getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(@Changeable(COLUMN_NAMES) ColumnNames columnNames) {
        this.columnNames = columnNames;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(@Changeable(START_DATE) Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(@Changeable(END_DATE) Date endDate) {
        this.endDate = endDate;
    }

    public Frequency getFrequency() {
        return frequency;
    }

    public void setFrequency(@Changeable(FREQUENCY) Frequency frequency) {
        this.frequency = frequency;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(@Changeable(ORDER) Order order) {
        this.order = order;
    }

    public Data getData() {
        return data;
    }

    public void setData(@Changeable(DATA) Data data) {
        this.data = data;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((collapse == null) ? 0 : collapse.hashCode());
        result = prime * result + ((columnIndex == null) ? 0 : columnIndex.hashCode());
        result = prime * result + ((columnNames == null) ? 0 : columnNames.hashCode());
        result = prime * result + ((data == null) ? 0 : data.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((frequency == null) ? 0 : frequency.hashCode());
        result = prime * result + ((limit == null) ? 0 : limit.hashCode());
        result = prime * result + ((order == null) ? 0 : order.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((transform == null) ? 0 : transform.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        DatasetData other = (DatasetData) obj;
        if (collapse != other.collapse)
            return false;
        if (columnIndex == null) {
            if (other.columnIndex != null)
                return false;
        } else if (!columnIndex.equals(other.columnIndex))
            return false;
        if (columnNames == null) {
            if (other.columnNames != null)
                return false;
        } else if (!columnNames.equals(other.columnNames))
            return false;
        if (data == null) {
            if (other.data != null)
                return false;
        } else if (!data.equals(other.data))
            return false;
        if (endDate == null) {
            if (other.endDate != null)
                return false;
        } else if (!endDate.equals(other.endDate))
            return false;
        if (frequency == null) {
            if (other.frequency != null)
                return false;
        } else if (!frequency.equals(other.frequency))
            return false;
        if (limit == null) {
            if (other.limit != null)
                return false;
        } else if (!limit.equals(other.limit))
            return false;
        if (order != other.order)
            return false;
        if (startDate == null) {
            if (other.startDate != null)
                return false;
        } else if (!startDate.equals(other.startDate))
            return false;
        if (transform != other.transform)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "DatasetData [limit=" + limit + ", transform=" + transform + ", columnNames=" + columnNames
            + ", startDate=" + startDate + ", endDate=" + endDate + ", columnIndex=" + columnIndex
            + ", collapse=" + collapse + ", frequency=" + frequency + ", order=" + order + ", data=" + data 
            + "]";
    }
}
