package com.coherentlogic.quandl.client.core.converters.timeseries;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.quandl.client.core.domain.timeseries.ColumnNames;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.reflection.ReflectionConverter;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.mapper.Mapper;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class ColumnNamesConverter extends ReflectionConverter {

    private static final Logger log = LoggerFactory.getLogger(ColumnNamesConverter.class);

    public ColumnNamesConverter(Mapper mapper, ReflectionProvider reflectionProvider) {
        super(mapper, reflectionProvider);
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        ColumnNames result = (ColumnNames) super.unmarshal(reader, context);

        log.debug("Adding the result to the context under the key: " + ColumnNames.COLUMN_NAMES +
            " with value: " + result);

        context.put(ColumnNames.COLUMN_NAMES, result);

        return result;
    }
}
