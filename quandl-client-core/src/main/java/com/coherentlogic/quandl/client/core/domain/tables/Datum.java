package com.coherentlogic.quandl.client.core.domain.tables;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.quandl.client.core.domain.NillableSpecification;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.converters.basic.StringConverter;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class Datum extends SerializableBean implements NillableSpecification {

    private static final long serialVersionUID = -1328625492968920053L;

    public static final String TYPE = "type", NIL = "nil", DATUM = "datum";

    @XStreamAlias(TYPE)
    @XStreamAsAttribute
    private String type;

    @XStreamAlias(NIL)
    @XStreamAsAttribute
    private Boolean nil;

    @XStreamConverter(StringConverter.class)
    private String value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public Boolean getNil() {
        return nil;
    }

    public void setNil(Boolean nil) {
        this.nil = nil;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public BigDecimal getValueAsBigDecimal () {
        return value == null ? null : new BigDecimal (value);
    }

    public Integer getValueAsInteger () {
        return value == null ? null : new Integer (value);
    }

    private final SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-mm-dd");

    public Date getValueAsDate () throws ParseException {
        return getValueAsDate (dateFormat);
    }

    public Date getValueAsDate (DateFormat dateFormat) throws ParseException {
        return value == null ? null : dateFormat.parse(value);
    }

    public java.sql.Date getValueAsSQLDate () throws ParseException {
        return getValueAsSQLDate (dateFormat);
    }

    public java.sql.Date getValueAsSQLDate (DateFormat dateFormat) throws ParseException {

        Date utilDate = getValueAsDate ();

        return utilDate == null ? null : new java.sql.Date (utilDate.getTime());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((dateFormat == null) ? 0 : dateFormat.hashCode());
        result = prime * result + ((nil == null) ? 0 : nil.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Datum other = (Datum) obj;
        if (dateFormat == null) {
            if (other.dateFormat != null)
                return false;
        } else if (!dateFormat.equals(other.dateFormat))
            return false;
        if (nil == null) {
            if (other.nil != null)
                return false;
        } else if (!nil.equals(other.nil))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Datum [type=" + type + ", nil=" + nil + ", value=" + value + "]";
    }
}
