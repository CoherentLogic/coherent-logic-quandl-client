package com.coherentlogic.quandl.client.core.domain.timeseries.data;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.quandl.client.core.exceptions.ParseRuntimeException;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class Row extends SerializableBean {

    private static final long serialVersionUID = -1479226675141354340L;

    private Map<String, String> columnMap = new HashMap<String, String> ();

    public BigDecimal getColumnAsBigDecimal (String column) {

        String value = columnMap.get(column);

        return (value == null || "".equals(value)) ? null : new BigDecimal (value);
    }

    private final DateFormat dateFormat = new SimpleDateFormat ("yyyy-mm-dd");

    public Date getColumnAsDate (String column) {

        final String value = columnMap.get(column);

        /**
         * TODO: The date should really always be a non-null value so should we bother with this?
         */
        if (value == null)
            throw new NullPointerException("The value for the column " + column + " is null; this: " + this);

        try {
            return dateFormat.parse(value);
        } catch (ParseException parseException) {
            throw new ParseRuntimeException ("Unable to convert the column " + column + " with value " +
                value + " to an instance of java.util.Date.", parseException);
        }
    }

    public Date getColumnAsDate (String column, DateFormat dateFormat) throws ParseException {
        return dateFormat.parse(columnMap.get(column));
    }

    /**
     * Method returns true iff there are values associated with all columns where values are non-null AND
     * also not equal to "".
     *
     * @param columns The columns that must have non-null values.
     *
     * @return True iff there are values associated with all columns.
     */
    public boolean hasValuesFor (String... columns) {

        boolean result = true;

        for(String nextColumn : columns) {

            String value = getColumn(nextColumn);

            if (value == null || "".equals(value)) {
                result = false;
                break;
            }
        }

        return result;
    }

    public void putColumn (String key, String value) {
        columnMap.put(key, value);
    }

    public String getColumn (String key) {
        return columnMap.get(key);
    }

    public void forEachColumn (Consumer<Entry<String, String>> consumer) {
        columnMap.entrySet().forEach(entry -> { consumer.accept(entry); } );
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((columnMap == null) ? 0 : columnMap.hashCode());
        result = prime * result + ((dateFormat == null) ? 0 : dateFormat.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Row other = (Row) obj;
        if (columnMap == null) {
            if (other.columnMap != null)
                return false;
        } else if (!columnMap.equals(other.columnMap))
            return false;
        if (dateFormat == null) {
            if (other.dateFormat != null)
                return false;
        } else if (!dateFormat.equals(other.dateFormat))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Row [columnMap=" + columnMap + ", dateFormat=" + dateFormat + "]";
    }
}
