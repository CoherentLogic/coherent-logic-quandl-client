package com.coherentlogic.quandl.client.core.builders.tables;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import javax.ws.rs.core.UriBuilder;

import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.quandl.client.core.builders.AbstractQuandlQueryBuilder;
import com.coherentlogic.quandl.client.core.domain.tables.QuandlResponse;


/**
 * 
 * @see <a href="https://docs.quandl.com/">Quandl API Documentation</a>
 * @see <a href="https://blog.quandl.com/getting-started-with-the-quandl-api">Getting Started with the Quandl
 *  API</a>
 * @see <a href="https://docs.quandl.com/docs/parameters-2">Times-series parameters</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class QueryBuilder extends AbstractQuandlQueryBuilder {

    public QueryBuilder(RestTemplate restTemplate) {
        super(restTemplate, DEFAULT_URI);
    }

    public QueryBuilder(RestTemplate restTemplate, String uri) {
        super(restTemplate, uri);
    }

    public QueryBuilder(RestTemplate restTemplate, UriBuilder uriBuilder) {
        super(restTemplate, uriBuilder);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, uri, cache);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, uriBuilder, cache);
    }

    @Override
    protected String getKey() {
        return getEscapedURI();
    }

    @Override
    public QueryBuilder withApiKey (String apiKey) {
        return (QueryBuilder) super.withApiKey (apiKey);
    }

//  https://docs.quandl.com/docs/quick-start-examples-9

    @Override
    public QueryBuilder withExternalApiKey() {
        return (QueryBuilder) super.withExternalApiKey();
    }

    @Override
    public QueryBuilder withExternalApiKey(String key) {
        return (QueryBuilder) super.withExternalApiKey(key);
    }

    public static final String TICKER = "ticker", MAPCODE = "mapcode", COMPNUMBER = "compnumber", REPORTTYPE = "reporttype";

    public QueryBuilder withTickers (String... tickers) {
        return withTickers (Arrays.asList(tickers));
    }

    public QueryBuilder withTickers (List<String> tickers) {
        return (QueryBuilder) addParameter(TICKER, ",", tickers);
    }

    public QueryBuilder withMapCode (String mapCode) {
      return (QueryBuilder) addParameter(MAPCODE, mapCode);
    }

    public QueryBuilder withMapCode (Integer mapCode) {
        return (QueryBuilder) addParameter(MAPCODE, mapCode);
    }

    public QueryBuilder withCompNumber (String compNumber) {
        return (QueryBuilder) addParameter(COMPNUMBER, compNumber);
    }

    public QueryBuilder withCompNumber (Integer compNumber) {
        return (QueryBuilder) addParameter(COMPNUMBER, compNumber);
    }

    public QueryBuilder withReportType (String reportType) {
        return (QueryBuilder) addParameter(REPORTTYPE, reportType);
    }

    static final String QOPTS_COLUMNS = "qopts.columns";

    public QueryBuilder withQOptsColumns (String... qOptsColumns) {
        return withQOptsColumns (Arrays.asList(qOptsColumns));
    }

    public QueryBuilder withQOptsColumns (List<String> qOptsColumns) {
        return (QueryBuilder) addParameter(QOPTS_COLUMNS, qOptsColumns);
    }

    static final String DATATABLES = "datatables";

    /**
     * Expands the path to include datatables -- see the example below:
     *
     * https://www.quandl.com/api/v3/datatables/ETFG/FUND.json?ticker=SPY&api_key=YOURAPIKEY
     *
     * @param databaseCode
     * @param datasetCode
     * @return
     */
    public QueryBuilder datatables (String databaseCode, String datasetCode) {

        extendPathWith (DATATABLES);
        extendPathWith (databaseCode);
        extendPathWith (notNull ("datasetCode", datasetCode).concat (".xml"));

        return this;
    }

    public QuandlResponse doGetAsQuandlResponse () {
        return doGetAsQuandlResponse (quandlResponse -> { return quandlResponse; });
    }

    /**
     * Do get as {@link QuandlResponse}, execute the given function, and then return an instance of type {@link Seriess}.
     */
    public QuandlResponse doGetAsQuandlResponse (Function<QuandlResponse, QuandlResponse> function) {
        return doGetAsQuandlResponse (QuandlResponse.class, function);
    }

    /**
     * Do get as {@link QuandlResponse}, execute the given function, and then return an instance of type resultType.
     */
    public <R> R doGetAsQuandlResponse (Class<R> resultType, Function<QuandlResponse, R> function) {

        QuandlResponse quandlResponse = doGet(QuandlResponse.class);

        R result = function.apply(quandlResponse);

        return result;
    }
}
