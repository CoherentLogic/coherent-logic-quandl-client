package com.coherentlogic.quandl.client.core.domain.timeseries;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public enum Order {

    asc, desc;
}
