package com.coherentlogic.quandl.client.core.domain.timeseries.metadata;

/**
 * @see <a href="https://blog.quandl.com/getting-started-with-the-quandl-api">Frequency</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public enum Frequency {

    none, daily, weekly, monthly, yearly, quarterly, annual
}
