package com.coherentlogic.quandl.client.core.builders.timeseries;

import java.util.Date;
import java.util.function.Function;

import javax.ws.rs.core.UriBuilder;

import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.adapter.core.command.CommandExecutorSpecification;
import com.coherentlogic.quandl.client.core.builders.AbstractQuandlQueryBuilder;
import com.coherentlogic.quandl.client.core.domain.timeseries.Collapse;
import com.coherentlogic.quandl.client.core.domain.timeseries.Order;
import com.coherentlogic.quandl.client.core.domain.timeseries.Transform;
import com.coherentlogic.quandl.client.core.domain.timeseries.data.QuandlResponse;

/**
 * Contains logic used by both the timeseries and metadata builder implementations.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 * @param <Q> The QueryBuilder type returned by the method being called -- facilitates method chaining flow.
 * @param <X> The specific QuandlResponse return type (could be either data or metadata).
 */
public abstract class AbstractTimeSeriesQueryBuilder<Q extends AbstractTimeSeriesQueryBuilder<Q, X>, X>
    extends AbstractQuandlQueryBuilder {

    public AbstractTimeSeriesQueryBuilder(
        RestTemplate restTemplate, String uri,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, uri, cache);
    }

    public AbstractTimeSeriesQueryBuilder(RestTemplate restTemplate, String uri) {
        super(restTemplate, uri);
    }

    public AbstractTimeSeriesQueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, uri, commandExecutor);
    }

    public AbstractTimeSeriesQueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, uriBuilder, cache);
    }

    public AbstractTimeSeriesQueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder
    ) {
        super(restTemplate, uriBuilder);
    }

    public AbstractTimeSeriesQueryBuilder(RestTemplate restTemplate) {
        super(restTemplate);
    }

    public AbstractTimeSeriesQueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<String> cache,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, uri, cache, commandExecutor);
    }

    public AbstractTimeSeriesQueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<String> cache,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, uriBuilder, cache, commandExecutor);
    }

    static final String DATASETS = "datasets";

    /**
     * Expands the path to include datasets -- see the example below:
     *
     * https://www.quandl.com/api/v3/datasets/WIKI/FB/
     *
     * @param databaseCode
     * @param datasetCode
     * @return
     */
    public Q datasets (String databaseCode, String datasetCode) {

        extendPathWith (DATASETS);
        extendPathWith (databaseCode);
        extendPathWith (datasetCode);

        return (Q) this;
    }

    static final String DOWNLOAD_TYPE = "download_type", PARTIAL = "partial", COMPLETE = "complete";

    public Q withDownloadType (String downloadType) {
        return (Q) addParameter(
            DOWNLOAD_TYPE,
            notNull (DOWNLOAD_TYPE, downloadType)
        );
    }

    public Q withDownloadType (DownloadType downloadType) {
        return withDownloadType (downloadType.toString());
    }

    public Q withDownloadTypeAsPartial () {
        return withDownloadType (DownloadType.partial);
    }

    public Q withDownloadTypeAsComplete () {
        return withDownloadType (DownloadType.complete);
    }

    static final String LIMIT = "limit";

    public Q withLimit (Integer limit) {
        return (Q) addParameter(
            LIMIT,
            notNegative (
                LIMIT,
                notNull (LIMIT, limit)
            )
        );
    }

    static final String COLUMN_INDEX = "column_index";

    public Q withColumnIndex (Integer columnIndex) {

        return (Q) addParameter(
            COLUMN_INDEX,
            notNegative (
                COLUMN_INDEX,
                notNull (COLUMN_INDEX, columnIndex)
            )
        );
    }

    static final String START_DATE = "start_date";

    public Q withStartDate (Date startDate) {
        return (Q) addParameter(START_DATE, startDate);
    }

    public Q withStartDate (String startDate) {
        return (Q) addParameter(START_DATE, startDate);
    }

    static final String END_DATE = "end_date";

    public Q withEndDate (Date endDate) {
        return (Q) addParameter(END_DATE, endDate);
    }

    public Q withEndDate (String endDate) {
        return (Q) addParameter(END_DATE, endDate);
    }

    static final String ORDER = "order";

    /**
     * Return data in ascending or descending order of date. Default is desc.
     *
     * @param order
     * @return
     */
    public Q withOrder (String order) {
        return (Q) addParameter(ORDER, order);
    }

    /**
     * Return data in ascending or descending order of date. Default is desc.
     *
     * @param order
     * @return
     */
    public Q withOrder (Order order) {
        return withOrder (order.toString());
    }

    /**
     * Return data in ascending order of date.
     *
     * @param order
     * @return
     */
    public Q withOrderAsAsc () {
        return withOrder (Order.asc);
    }

    /**
     * Return data in ascending order of date.
     *
     * @param order
     * @return
     */
    public Q withOrderAsDesc () {
        return withOrder (Order.desc);
    }

    static final String COLLAPSE = "collapse";

    /**
     * 
     *
     * @param 
     * @return
     */
    public Q withCollapse (String collapse) {
        return (Q) addParameter(COLLAPSE, collapse);
    }

    /**
     * @see {@link Collapse}
     *
     * @param 
     * @return
     */
    public Q withCollapse (Collapse collapse) {
        return withCollapse (collapse.toString());
    }

    /**
     * @see #withCollapse(Collapse)
     */
    public Q withCollapseAsNone () {
        return withCollapse (Collapse.none);
    }

    /**
     * @see #withCollapse(Collapse)
     */
    public Q withCollapseAsAnnual () {
        return withCollapse (Collapse.annual);
    }

    /**
     * @see #withCollapse(Collapse)
     */
    public Q withCollapseAsDaily () {
        return withCollapse (Collapse.daily);
    }

    /**
     * @see #withCollapse(Collapse)
     */
    public Q withCollapseAsMonthly () {
        return withCollapse (Collapse.monthly);
    }

    /**
     * @see #withCollapse(Collapse)
     */
    public Q withCollapseAsQuarterly () {
        return withCollapse (Collapse.quarterly);
    }

    /**
     * @see #withCollapse(Collapse)
     */
    public Q withCollapseAsWeekly () {
        return withCollapse (Collapse.weekly);
    }

    static final String TRANSFORM = "transform";

    public Q withTransform (String transform) {
        return (Q) addParameter(TRANSFORM, transform);
    }

    /**
     * @see {@link Transform}
     */
    public Q withTransform (Transform transform) {
        return withTransform (transform.toString());
    }

    /**
     * @see #withTransform(String)
     */
    public Q withTransformAsNone () {
        return withTransform (Transform.none);
    }

    /**
     * @see #withTransform(String)
     */
    public Q withTransformAsDiff () {
        return withTransform (Transform.diff);
    }

    /**
     * @see #withTransform(String)
     */
    public Q withTransformAsRDiff () {
        return withTransform (Transform.rdiff);
    }

    /**
     * @see #withTransform(String)
     */
    public Q withTransformAsRDiffFrom () {
        return withTransform (Transform.rdiff_from);
    }

    /**
     * @see #withTransform(String)
     */
    public Q withTransformAsCumul () {
        return withTransform (Transform.cumul);
    }

    /**
     * @see #withTransform(String)
     */
    public Q withTransformAsNormalize () {
        return withTransform (Transform.normalize);
    }

    /**
     * Do get as {@link QuandlResponse}, execute the given function, and then return an instance of type X.
     */
    public X doGetAsQuandlResponse () {
        return doGetAsQuandlResponse (quandlResponse -> { return quandlResponse; });
    }

    /**
     * Do get as {@link QuandlResponse}, execute the given function, and then return an instance of type X.
     */
    public abstract X doGetAsQuandlResponse (Function<X, X> function);

    /**
     * Do get as {@link QuandlResponse}, execute the given function, and then return an instance of type resultType.
     */
    public abstract <R> R doGetAsQuandlResponse (Class<R> resultType, Function<X, R> function);
}
