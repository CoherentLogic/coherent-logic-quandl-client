package com.coherentlogic.quandl.client.core.domain.timeseries.data;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * A column with a type and values.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=Datum.DATUM)
@XStreamAlias(Datum.DATUM)
@Visitable
public class Datum extends SerializableBean {

    private static final long serialVersionUID = -2477270990046020088L;

    // Should this be an enum?
    private String type;

    private List<String> values = new ArrayList<String> ();

    static final String DATUM = "datum", DATUM_LIST = "datumList", TYPE = "type", VALUES = "values";

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(@Changeable (VALUES) List<String> values) {
        this.values = values;
    }

    public void addValue(String value) {
        values.add(value);
    }

    public String getValueAt (int index) {
        return values.get(index);
    }

    public int size () {
        return values.size ();
    }

    public BigDecimal getValueAtAsBigDecimal (int index) {
        return new BigDecimal (getValueAt (index));
    }

    private final SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-mm-dd");

    public Date getValueAtAsDate (int index) throws ParseException {
        return dateFormat.parse(getValueAt (index));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((dateFormat == null) ? 0 : dateFormat.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((values == null) ? 0 : values.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Datum other = (Datum) obj;
        if (dateFormat == null) {
            if (other.dateFormat != null)
                return false;
        } else if (!dateFormat.equals(other.dateFormat))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        if (values == null) {
            if (other.values != null)
                return false;
        } else if (!values.equals(other.values))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Datum [type=" + type + ", values=" + values + "]";
    }
}
