package com.coherentlogic.quandl.client.core.domain.timeseries.metadata;

import java.util.Collection;
import java.util.function.Consumer;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.coherent.data.model.core.strategies.TraversalStrategySpecification;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @see <a href="https://docs.quandl.com/docs/in-depth-usage#section-get-time-series-metadata">Get time-series metadata</a>
 * @see <a href="https://www.quandl.com/api/v3/datasets/WIKI/FB/metadata.xml">Metadata</a>
 */
@Entity
@Table(name = QuandlResponse.TIME_SERIES_METADATA_TABLE_NAME)
@XStreamAlias(QuandlResponse.TIME_SERIES_METADATA_QUANDL_RESPONSE_ALIAS)
@Visitable
public class QuandlResponse extends SerializableBean {

    private static final long serialVersionUID = 4521945783542773807L;

    static final String TIME_SERIES_METADATA_TABLE_NAME = "time_series_metadata_quandl_response";

    static final String TIME_SERIES_METADATA_QUANDL_RESPONSE_ALIAS = "quandl-response";

    @XStreamAlias(Dataset.DATASET)
    private Dataset dataset;

    private static final TraversalStrategySpecification defaultTraversalStrategy
        = new TraversalStrategySpecification () {

        @Override
        public void execute(
            SerializableBean serializableBean,
            Collection<Consumer<SerializableBean>> visitors
        ) {

            QuandlResponse quandlResponse = (QuandlResponse) serializableBean;

            if (quandlResponse.dataset != null)
                quandlResponse.dataset.accept(visitors);

            visitors.forEach(visitor -> { visitor.accept(quandlResponse); } );
        }
    };

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {
        accept(defaultTraversalStrategy, visitors);
    }

    public Dataset getDataset() {
        return dataset;
    }

    public void setDataset(@Changeable (Dataset.DATASET) Dataset dataset) {
        this.dataset = dataset;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((dataset == null) ? 0 : dataset.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        QuandlResponse other = (QuandlResponse) obj;
        if (dataset == null) {
            if (other.dataset != null)
                return false;
        } else if (!dataset.equals(other.dataset))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "QuandlResponse [dataset=" + dataset + "]";
    }
}
