package com.coherentlogic.quandl.client.core.builders;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

/**
 * 
 * @author thospfuller
 */
public class AbstractQuandlQueryBuilderTest {

    class QuandlQueryBuilder extends AbstractQuandlQueryBuilder {

        public QuandlQueryBuilder(RestTemplate restTemplate) {
            super(restTemplate);
        }
    }

    private QuandlQueryBuilder queryBuilder = null;

    @Before
    public void setUp() throws Exception {
        queryBuilder = new QuandlQueryBuilder (null);
    }

    @After
    public void tearDown() throws Exception {
        queryBuilder = null;
    }

    @Test
    public void testWithApiKey() {

        queryBuilder.withApiKey("foo");

        assertEquals ("https://www.quandl.com/api/v3/?api_key=foo", queryBuilder.getEscapedURI());
    }

    @Test
    public void testMetadata() {

        String actual = queryBuilder.metadata().getEscapedURI();

        assertEquals ("https://www.quandl.com/api/v3/metadata.xml", actual);
    }

    @Test
    public void testData() {

        String actual = queryBuilder.data().getEscapedURI();

        assertEquals ("https://www.quandl.com/api/v3/data.xml", actual);
    }

}
