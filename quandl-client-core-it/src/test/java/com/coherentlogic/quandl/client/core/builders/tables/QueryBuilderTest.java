package com.coherentlogic.quandl.client.core.builders.tables;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.coherentlogic.quandl.client.core.domain.tables.Column;
import com.coherentlogic.quandl.client.core.domain.tables.Columns;
import com.coherentlogic.quandl.client.core.domain.tables.Data;
import com.coherentlogic.quandl.client.core.domain.tables.DataTable;
import com.coherentlogic.quandl.client.core.domain.tables.DatumArray;
import com.coherentlogic.quandl.client.core.domain.tables.Meta;
import com.coherentlogic.quandl.client.core.domain.tables.QuandlResponse;

/**
 * https://www.quandl.com/api/v3/datatables/ETFG/FUND.xml?ticker=SPY,IWM,GLD&qopts.columns=ticker,date,shares_outstanding&api_key=YOURAPIKEY *
 */
public class QueryBuilderTest {

    private final ApplicationContext context
        = new FileSystemXmlApplicationContext ("src/test/resources/spring/application-context.xml");

    private final String apiKey = System.getProperty("QUANDL_API_KEY");

    /**
     * https://www.quandl.com/api/v3/datasets/WIKI/FB/data.xml?api_key=YOURAPIKEY
     */
//    @Test
    public void testMERF1() {

        QueryBuilder queryBuilder = context.getBean(QueryBuilder.class);

        // https://docs.quandl.com/docs/in-depth-usage-1
        // https://www.quandl.com/api/v3/datatables/MER/F1.xml?&mapcode=-5370&compnumber=39102&reporttype=A&qopts.columns=reportdate,amount&api_key=[redacted]
        QuandlResponse result = queryBuilder
            .datatables("MER", "F1")
            .withExternalApiKey()
            .doGetAsQuandlResponse ();

        System.out.println("queryBuilder.xml: " + queryBuilder.getEscapedURI());

        assertEquals (10000, result.getDataTable().getData().getDatumArrayList().size());

        /*
         * <datum type="array">
         *   <datum type="date">2014-01-06</datum>
         *   <datum>SPY</datum>
         *   <datum type="float">954182116.0</datum>
         *   <datum type="float">182.422859</datum>
         *   <datum type="float">310894607.7</datum>
         * </datum>
         */

        DataTable dataTable = result.getDataTable();

        Data data = dataTable.getData();

        assertEquals ("array", data.getType());

        DatumArray datumArray = data.getDatumArrayList().get(2);

        assertEquals ("date", datumArray.getDatumAt(0).getType());
        assertEquals ("2014-01-06", datumArray.getDatumAt(0).getValue());
        assertEquals ("SPY", datumArray.getDatumAt(1).getValue());
        assertEquals ("float", datumArray.getDatumAt(2).getType());
        assertEquals ("954182116.0", datumArray.getDatumAt(2).getValue());

        assertEquals ("float", datumArray.getDatumAt(3).getType());
        assertEquals ("182.422859", datumArray.getDatumAt(3).getValue());
        assertEquals ("float", datumArray.getDatumAt(4).getType());
        assertEquals ("310894607.7", datumArray.getDatumAt(4).getValue());

        /*
         * <columns type="array">
         *   <column>
         *     <name>date</name>
         *     <type>Date</type>
         *   </column>
         *   ...
         * </columns>
         */
        Columns columns = dataTable.getColumns();

        assertEquals ("array", columns.getType ());

        assertEquals (5, columns.size());

        Column column = columns.getColumnAtIndex(0);

        assertEquals ("date", column.getName ());
        assertEquals ("Date", column.getType ());

        Meta meta = result.getMeta();

        meta.getNextCursorId();

////        // https://docs.quandl.com/docs/in-depth-usage-1
////        // https://www.quandl.com/api/v3/datatables/ETFG/FUND.json?
////        // ticker=SPY,IWM,GLD&qopts.columns=ticker,date,shares_outstanding
////        // &api_key=YOURAPIKEY
//        QuandlResponse result = queryBuilder
//            .datatables("ETFG", "FUND")
//            .withTickers ("SPY", "IWM", "GLD")
//            .withQOptsColumns("ticker", "date", "shares_outstanding")
//            .withExternalApiKey()
//            .doGetAsQuandlResponse ();
    }

    /**
     * //  https://www.quandl.com/api/v3/datatables/MER/F1.xml?&mapcode=-5370&compnumber=39102&reporttype=A&qopts.columns=reportdate,amount&api_key=<YOURAPIKEY>"
     */
//    @Test
    public void testETFGFUND () {

        QueryBuilder queryBuilder = context.getBean(QueryBuilder.class);

        String result = queryBuilder
                .datatables("ETFG", "FUND")
                .withTickers("SPY")
                .withExternalApiKey()
                .doGetAsString();

        System.out.println("uri: " + queryBuilder.getEscapedURI());
    }
}
