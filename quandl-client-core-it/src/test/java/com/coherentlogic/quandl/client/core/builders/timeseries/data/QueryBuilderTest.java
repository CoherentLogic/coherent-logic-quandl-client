package com.coherentlogic.quandl.client.core.builders.timeseries.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.coherentlogic.quandl.client.core.domain.timeseries.Collapse;
import com.coherentlogic.quandl.client.core.domain.timeseries.ColumnNames;
import com.coherentlogic.quandl.client.core.domain.timeseries.Order;
import com.coherentlogic.quandl.client.core.domain.timeseries.data.Data;
import com.coherentlogic.quandl.client.core.domain.timeseries.data.DatasetData;
import com.coherentlogic.quandl.client.core.domain.timeseries.data.QuandlResponse;
import com.coherentlogic.quandl.client.core.domain.timeseries.metadata.Frequency;

public class QueryBuilderTest {

    private final ApplicationContext context
        = new FileSystemXmlApplicationContext ("src/test/resources/spring/application-context.xml");

    class Result {

        private Date date;

        private BigDecimal open;

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public BigDecimal getOpen() {
            return open;
        }

        public void setOpen(BigDecimal open) {
            this.open = open;
        }

        @Override
        public String toString() {
            return "Result [date=" + date + ", open=" + open + "]";
        }
    }

//    /**
//     * https://www.quandl.com/api/v3/datasets/WIKI/FB/data.xml?api_key=YOURAPIKEY
//     */
//    @Test
//    public void testWIKIFB() {
//
//        QueryBuilder queryBuilder = context.getBean(QueryBuilder.class);
//
//        QuandlResponse quandlResponse = queryBuilder
//            .datasets("WIKI", "FB")
//            .data()
//            .withCollapseAsMonthly()
//            .withLimit(0)
//            .withColumnIndex(1)
//            .withStartDate("2010-01-01")
//            .withEndDate("2016-01-01")
//            .withExternalApiKey()
//            .doGetAsQuandlResponse ();
//
//        System.out.println("quandlResponse: " + quandlResponse);
//
//        final List<Result> results = new ArrayList<Result> ();
//
//        quandlResponse.getDatasetData()
//            .getData()
//            .forEachRow(
//                row -> {
//
//                    Result nextResult = new Result ();
//
//                    nextResult.setDate(row.getColumnAsDate("Date"));
//                    nextResult.setOpen(row.getColumnAsBigDecimal("Open"));
//
//                    results.add(nextResult);
//                }
//            );
//
//        AtomicInteger ctr = new AtomicInteger (0);
//
//        results.forEach(
//            next -> {
//                System.out.println("next @ " + ctr.getAndIncrement() + ": " + next);
//            }
//        );
//
//        System.out.println("uri: " + queryBuilder.getEscapedURI());
//    }

    /**
     * -DQUANDL_API_KEY=
     * https://docs.quandl.com/docs/quick-start-examples-1
     * https://www.quandl.com/api/v3/datasets/FRED/GDP.csv?collapse=annual&rows=6&order=asc&column_index=1&api_key=[YOURAPIKEY]
     */
    @Test
    public void testFREDGDP() {

        QueryBuilder queryBuilder = context.getBean(QueryBuilder.class);

        QuandlResponse result = queryBuilder
            .datasets("FRED", "GDP")
            .data()
            .withCollapseAsAnnual()
            .withLimit(6)
            .withOrderAsAsc()
            .withColumnIndex(1)
            .withExternalApiKey()
            .doGetAsQuandlResponse ();

        System.out.println("result: " + queryBuilder.getEscapedURI());

        DatasetData datasetData = result.getDatasetData();

        assertNotNull (datasetData);

        assertEquals (Integer.valueOf(6), datasetData.getLimit());
        assertNull (datasetData.getTransform());

        assertEquals (Integer.valueOf(1), datasetData.getColumnIndex());

        ColumnNames columnNames = datasetData.getColumnNames();

        assertEquals (2, columnNames.size());

        assertEquals ("Date", columnNames.getColumnNameAtIndex(0));
        assertEquals ("Value", columnNames.getColumnNameAtIndex(1));

        assertEquals ("Wed Jan 01 06:00:00 UTC 1947", datasetData.getStartDate().toString());
        assertEquals ("Sun Jul 01 06:00:00 UTC 2018", datasetData.getEndDate().toString());
        assertEquals (Frequency.quarterly, datasetData.getFrequency());

        Data data = datasetData.getData();

        assertEquals (Integer.valueOf(6), data.getDatumSize().get("Date"));
        assertEquals (Integer.valueOf(6), data.getDatumSize().get("Value"));

        assertEquals ("1947-12-31", data.getDatumByColumnName("Date").getValueAt(0));
        assertEquals ("259.745", data.getDatumByColumnName("Value").getValueAt(0));

        assertEquals (Collapse.annual, datasetData.getCollapse());
        assertEquals (Order.asc, datasetData.getOrder ());
    }

//    /**
//     * https://www.quandl.com/data/EOD-End-of-Day-US-Stock-Prices/usage/quickstart/api
//     *
//     * https://www.quandl.com/api/v3/datasets/EOD/HD/data.xml?start_date=2018-05-18&end_date=2018-05-18&api_key=[YOURAPIKEY]
//     */
//    @Test
//    public void testEODHD() {
//
//        QueryBuilder queryBuilder = context.getBean(QueryBuilder.class);
//
//        QuandlResponse result = queryBuilder
//            .datasets("EOD", "HD")
//            .data ()
//            .withExternalApiKey()
//            .doGetAsQuandlResponse ();
//
//        ColumnNames columnNames = result.getDatasetData().getColumnNames();
//
//        columnNames.getColumnNames().forEach(columnName -> { System.out.print(columnName + "\t\t"); });
//
//        System.out.println();
//
//        result.getDatasetData().getData().forEachRow(row -> {
//
//            columnNames.getColumnNames().forEach(columnName -> { System.out.print(row.getColumn(columnName) + "\t\t"); });
//
//            System.out.println();
//        });
//    }
}
