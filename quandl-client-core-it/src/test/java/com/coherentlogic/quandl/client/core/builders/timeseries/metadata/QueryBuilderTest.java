package com.coherentlogic.quandl.client.core.builders.timeseries.metadata;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.coherentlogic.quandl.client.core.domain.timeseries.ColumnNames;
import com.coherentlogic.quandl.client.core.domain.timeseries.metadata.Dataset;
import com.coherentlogic.quandl.client.core.domain.timeseries.metadata.Frequency;
import com.coherentlogic.quandl.client.core.domain.timeseries.metadata.QuandlResponse;

public class QueryBuilderTest {

    private final ApplicationContext context
        = new FileSystemXmlApplicationContext ("src/test/resources/spring/application-context.xml");

    /**
     * https://www.quandl.com/api/v3/datatables/MER/F1?compnumber=372&reportdate=2010-12-31%2C2011-03-31%2C2011-06-30&api_key=[redacted]
     */
    @Test
    public void testMERF1Metadata() {

        QueryBuilder queryBuilder =
            context.getBean(QueryBuilder.class);

        QuandlResponse result = queryBuilder
            .datasets("EOD", "HD")
            .metadata()
            .withExternalApiKey()
            .doGetAsQuandlResponse();

        System.out.println("uri: " + queryBuilder.getEscapedURI());

        Dataset dataset = result.getDataset();

        assertEquals(Integer.valueOf(42635437), dataset.getId());
        assertEquals( "HD", dataset.getDatasetCode());
        assertEquals("EOD", dataset.getDatabaseCode());

        assertEquals("Home Depot Inc. (The) (HD) Stock Prices, Dividends and Splits", dataset.getName());
        assertNotNull(dataset.getDescription());

        assertNotNull(dataset.getRefreshedAt());
        assertNotNull(dataset.getNewestAvailableDate());
        // The string changes so I'm going to assume that if it's not null and not blank that it's been set
        // properly.
        assertNotEquals("", dataset.getNewestAvailableDate().toString());

        assertNotNull(dataset.getOldestAvailableDate());

        // The string changes so I'm going to assume that if it's not null and not blank that it's been set
        // properly.
        assertNotEquals("", dataset.getOldestAvailableDate().toString());

        ColumnNames columnNames = dataset.getColumnNames();

        assertEquals(13, columnNames.size());

        assertEquals("Date", columnNames.getColumnNameAtIndex(0));
        assertEquals("Adj_Volume", columnNames.getColumnNameAtIndex(12));

        assertEquals(Frequency.daily, dataset.getFrequency());
        assertEquals("Time Series", dataset.getType());
        assertEquals(Boolean.TRUE, dataset.getPremium());
        assertEquals(Integer.valueOf(12910), dataset.getDatabaseId());
    }

//    /**
//     * https://www.quandl.com/api/v3/datasets/EOD/HD?start_date=2018-05-18&end_date=2018-05-18&api_key=[redacted]
//     */
//    @Test
//    public void testEODHD() {
//
//        QueryBuilder queryBuilder = context.getBean(QueryBuilder.class);
//
//        QuandlResponse result = queryBuilder
//            .datasets("EOD", "HD")
//            .withStartDate("2017-05-18")
//            .withEndDate("2018-05-18")
//            .withExternalApiKey()
//            .metadata()
//            .doGetAsQuandlResponse ();
//
//        System.out.println("result: " + result);
//    }
}
