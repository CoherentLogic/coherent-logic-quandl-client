# Coherent Logic Quandl Client

## See Also

- [FRED Client](https://coherentlogic.com/wordpress/middleware-development/are-you-looking-to-retrieve-economic-data-from-the-federal-reserve-bank-of-st-louis/)
- [Coherent Data Adapter: US Treasury Direct Client](https://coherentlogic.com/wordpress/middleware-development/us-treasury-direct-client/)
- [World Bank Client](https://coherentlogic.com/wordpress/middleware-development/world-bank-client/)
- [Coherent Data Adapter: OpenFIGI Client Edition](https://coherentlogic.com/wordpress/middleware-development/coherent-data-adapter-openfigi-edition/)
- [Coherent Data Adapter: Quandl Client Edition](https://coherentlogic.com/wordpress/middleware-development/quandl-client/)
- [USA Spending Client](https://coherentlogic.com/wordpress/middleware-development/usa-spending-client/)
- [Coherent Datafeed: Thomson Reuters Edition](https://coherentlogic.com/wordpress/middleware-development/solutions-for-traders-using-thomson-reuters-market-data/)
- [Coherent Data Adapter: CUSIP Global Services Web Edition](https://coherentlogic.com/wordpress/middleware-development/coherent-data-adapter-cusip-global-services-web-edition/)
- [CMR: Rapid Data Acquisition for the Apache Spark Cluster Computing Platform](https://coherentlogic.com/wordpress/middleware-development/cmr/)
- [Coherent Logic Enterprise Data Adapter](https://coherentlogic.com/wordpress/middleware-development/enterprise-data-adapter/)


