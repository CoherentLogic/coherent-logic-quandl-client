package com.coherentlogic.quandl.client.examples.multithreaded.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEvent.EventType;
import com.coherentlogic.quandl.client.core.builders.timeseries.data.QueryBuilder;
import com.coherentlogic.quandl.client.core.domain.timeseries.data.Datum;
import com.coherentlogic.quandl.client.core.domain.timeseries.data.QuandlResponse;

import joinery.DataFrame;
import joinery.DataFrame.PlotType;

public class Main {

    static class Result {

        private Date date;

        private BigDecimal value;

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public BigDecimal getValue() {
            return value;
        }

        public void setValue(BigDecimal value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Result [date=" + date + ", value=" + value + "]";
        }
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        final FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext (
                "src/main/resources/spring/multithreaded-example/application-context.xml");

        QueryBuilder queryBuilder = context.getBean(QueryBuilder.class);

        Function<QueryBuilder, QuandlResponse> function = (fQueryBuilder) -> {

            String result = fQueryBuilder.datasets("NASDAQOMX", "XQC")
            .data()
            .withExternalApiKey()
            .doGetAsString();

            System.out.println("result: " + result);

            return fQueryBuilder.doGetAsQuandlResponse();
        };

        queryBuilder.addQueryBuilderEventListener(
            event -> {

                if (EventType.methodEnds.equals(event.getEventType())) {

                    QuandlResponse eventDrivenResponse = (QuandlResponse) event.getValue();

                    System.out.println("uri: " + queryBuilder.getEscapedURI() + ", queryBuilder: " + queryBuilder);

                    final List<Result> results = new ArrayList<Result> ();

                    final DataFrame<Object> dataFrame = new DataFrame<Object> ();

                    Datum dateDatum = eventDrivenResponse.getDatasetData()
                        .getData().getDatumByColumnName("Trade Date");

                    List<String> dateList = dateDatum.getValues();

                    List<Object> dateObjectList = new ArrayList<Object> ();

                    Datum valueDatum = eventDrivenResponse.getDatasetData()
                        .getData().getDatumByColumnName("Index Value");

                    List<String> valueStringList = valueDatum.getValues();

                    List<BigDecimal> valueList = new ArrayList<BigDecimal> ();

                    Collections.reverse(dateList);
                    Collections.reverse(valueList);

                    List<Object> valueObjectList = (List<Object>) (Object) valueList;

                    AtomicInteger ai = new AtomicInteger (0);

                    valueStringList.forEach(item -> {

                        if (!(item == null || "".equals(item))) {

                            valueList.add(new BigDecimal (item));

                            dateObjectList.add(dateList.get(ai.get()));
                        }

                        ai.incrementAndGet();
                    });

                    Collections.reverse(valueList);

                    dataFrame.add((Object) "Date", dateObjectList);
                    dataFrame.add((Object) "Value", valueObjectList);

                    dataFrame.plot(PlotType.LINE);
                    dataFrame.show();
                }
            }
        );

        Future<QuandlResponse> future = queryBuilder.invoke(function);

        QuandlResponse quandlResponse = future.get ();

        System.out.println("quandlResponse: " + quandlResponse);

        context.close();
    }
}
