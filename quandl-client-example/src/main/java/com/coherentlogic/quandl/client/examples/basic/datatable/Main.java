package com.coherentlogic.quandl.client.examples.basic.datatable;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.coherentlogic.quandl.client.core.builders.tables.QueryBuilder;
import com.coherentlogic.quandl.client.core.domain.tables.Column;
import com.coherentlogic.quandl.client.core.domain.tables.Columns;
import com.coherentlogic.quandl.client.core.domain.tables.Datum;
import com.coherentlogic.quandl.client.core.domain.tables.DatumArray;
import com.coherentlogic.quandl.client.core.domain.tables.QuandlResponse;

import joinery.DataFrame;
import joinery.DataFrame.PlotType;

public class Main {

    static class Result {

        private Date date;

        private BigDecimal open;

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public BigDecimal getOpen() {
            return open;
        }

        public void setOpen(BigDecimal open) {
            this.open = open;
        }

        @Override
        public String toString() {
            return "Result [date=" + date + ", open=" + open + "]";
        }
    }

    public static void main(String[] args) throws IOException {

        final FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext (
                "src/main/resources/spring/basic-example/application-context.xml");

        QueryBuilder queryBuilder = context.getBean(QueryBuilder.class);

        // Mergent Global Fundamentals Data / MER F1
        // https://www.quandl.com/api/v3/datatables/MER/F1.xml?api_key=[REDACTED]

        queryBuilder
            .datatables("MER", "F1")
            .withExternalApiKey();

        System.out.println("uri: " + queryBuilder.getEscapedURI());

        QuandlResponse quandlResponse = queryBuilder.doGetAsQuandlResponse ();

        Columns columns = quandlResponse
            .getDataTable()
            .getColumns();

        Map<String, Column> columnMap = columns.asMap();

        Column mapCodeColumn = columnMap.get("mapcode");
        Column longNameColumn = columnMap.get("longname");
        Column reportDateColumn = columnMap.get("reportdate");
        Column amountColumn = columnMap.get("amount");

        int mapCodeIndex = columns.getIndexOf(mapCodeColumn);
        int longNameIndex = columns.getIndexOf(longNameColumn);
        int reportDateIndex = columns.getIndexOf(reportDateColumn);
        int amountIndex = columns.getIndexOf(amountColumn);

        final List<Object> dateList = new ArrayList<Object> ();
        final List<Object> amountList = new ArrayList<Object> ();

        quandlResponse
            .getDataTable()
            .getData()
            .getDatumArrayList()
            .forEach((DatumArray datumArray) -> {

                Datum mapCodeDatum = datumArray.getDatumAt(mapCodeIndex);
                Datum longNameDatum = datumArray.getDatumAt(longNameIndex);

                if ("-9087".equals(mapCodeDatum.getValue())
                    &&
                    "RenaissanceRe Holdings Ltd.".equals (longNameDatum.getValue())) {

                    Datum reportDateDatum = datumArray.getDatumAt(reportDateIndex);
                    Datum amountDatum = datumArray.getDatumAt(amountIndex);

                    String reportDate = reportDateDatum.getValue();
                    BigDecimal amount = amountDatum.getValueAsBigDecimal();

                    if (amount != null) {
                        dateList.add(reportDate);
                        amountList.add(amount);
                    }
                }
        });

        final DataFrame<Object> dataFrame = new DataFrame<Object> ();

        dataFrame.add((Object) "Report Date", dateList);
        dataFrame.add((Object) "Amount", amountList);

        dataFrame.plot(PlotType.LINE);
        dataFrame.show();

        context.close();
    }
}
