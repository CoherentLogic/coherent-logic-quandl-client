package com.coherentlogic.quandl.client.examples.cached.multithreaded.data;

import java.util.concurrent.TimeUnit;

import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.Configuration;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.transaction.TransactionMode;
import org.infinispan.configuration.global.GlobalConfiguration;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.transaction.LockingMode;
import org.infinispan.transaction.lookup.EmbeddedTransactionManagerLookup;
import org.infinispan.util.concurrent.IsolationLevel;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class ApplicationConfiguration {

    public static final String
        QUANDL_CACHE_MANAGER = "quandlCacheManager",
        DEFAULT_CACHE_NAME = "defaultCacheName",
        QUANDL_CACHE = "quandlCache",
        INFINISPAN_DEFAULT_CACHE_FACTORY_BEAN = "infinispanDefaultCacheFactoryBean",
        TRANSACTION_MANAGER = "transactionManager";

    @Bean(name = DEFAULT_CACHE_NAME)
    public String getDefaultCacheName() {
        return QUANDL_CACHE;
    }

    /**
     * 
     * @see https://spring.io/blog/2011/08/15/configuring-spring-and-jta-without-full-java-ee/
     * @see https://github.com/jbosstm/quickstart/tree/master/jca-and-hibernate
     */
    @Bean(name = QUANDL_CACHE_MANAGER)
    public EmbeddedCacheManager getDefaultCacheManager() {

        GlobalConfigurationBuilder globalConfigurationBuilder =
            GlobalConfigurationBuilder.defaultClusteredBuilder();

        GlobalConfiguration globalConfiguration = globalConfigurationBuilder
            .transport()
            .addProperty("configurationFile", "spring/cached-multithreaded-example/infinispan-default-jgroups-tcp.xml")
            .clusterName("QuandlClientCluster")
            .defaultTransport()
            .defaultCacheName("QuandlClientCache")
            .build();

        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();

        Configuration configuration = configurationBuilder
            .clustering()
                .cacheMode(CacheMode.DIST_SYNC)
                .l1()
                .lifespan(60000L * 5L)
                .transaction()
                    .transactionMode(TransactionMode.TRANSACTIONAL)
                    .autoCommit(false)
                    .transactionManagerLookup(
                        new EmbeddedTransactionManagerLookup()
                    )
                    .lockingMode(LockingMode.PESSIMISTIC)
                    .locking()
                        .concurrencyLevel(10)
                        .isolationLevel(IsolationLevel.READ_COMMITTED)
                        .lockAcquisitionTimeout(1L, TimeUnit.MINUTES)
                        .clustering()
                    .build();

                    //.jmxStatistics().build();

        DefaultCacheManager result = new DefaultCacheManager(globalConfiguration, configuration);

//        result.addListener(new QuandlClusterAndCacheListener());

        return result;
    }
}