package com.coherentlogic.quandl.client.examples.basic.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.coherentlogic.quandl.client.core.builders.timeseries.data.QueryBuilder;
import com.coherentlogic.quandl.client.core.domain.timeseries.data.Datum;
import com.coherentlogic.quandl.client.core.domain.timeseries.data.QuandlResponse;

import joinery.DataFrame;
import joinery.DataFrame.PlotType;

public class Main {

    static class Result {

        private Date date;

        private BigDecimal value;

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public BigDecimal getValue() {
            return value;
        }

        public void setValue(BigDecimal value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Result [date=" + date + ", value=" + value + "]";
        }
    }

    public static void main(String[] args) {

        final FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext (
                "src/main/resources/spring/basic-example/application-context.xml");

        QueryBuilder queryBuilder = context.getBean(QueryBuilder.class);

        QuandlResponse quandlResponse = queryBuilder
            .datasets("CHRIS", "ICE_B1")
            .data()
            .withExternalApiKey()
            .doGetAsQuandlResponse ();

//        quandlResponse.accept(value -> {
//            System.out.println("value: " + value.getClass());
//        });

//        System.out.println("uri: " + queryBuilder.getEscapedURI());
//        System.out.println("quandlResponse.datasetData.order: " + quandlResponse.getDatasetData().getOrder() + ", frequency: " + quandlResponse.getDatasetData().getFrequency());

        final List<Result> results = new ArrayList<Result> ();

        final DataFrame<Object> dataFrame = new DataFrame<Object> ();

        Datum dateDatum = quandlResponse.getDatasetData()
            .getData().getDatumByColumnName("Date");

        List<String> dateList = new ArrayList<String> (); //dateDatum.getValues();

        List<Object> dateObjectList = (List<Object>) (Object) dateList;

        Datum valueDatum = quandlResponse.getDatasetData()
            .getData().getDatumByColumnName("Open");

        List<String> valueStringList = valueDatum.getValues();

        List<BigDecimal> valueList = new ArrayList<BigDecimal> ();

        List<Object> valueObjectList = (List<Object>) (Object) valueList;

        AtomicInteger ctr = new AtomicInteger (0);

        valueStringList.forEach(value -> {

            if (value != null && !"".equals(value)) {
                dateList.add(dateDatum.getValues().get(ctr.get()));
                valueList.add(new BigDecimal (value));
            }

            ctr.incrementAndGet();
        });

        Collections.reverse(dateList);
        Collections.reverse(valueList);

        dataFrame.add((Object) "Date", dateObjectList);
        dataFrame.add((Object) "Value", valueObjectList);

        dataFrame.plot(PlotType.LINE);
        dataFrame.show();

        context.close();
    }
}
