package com.coherentlogic.quandl.client.examples.cached.multithreaded.data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEvent.EventType;
import com.coherentlogic.quandl.client.core.builders.timeseries.data.QueryBuilder;
import com.coherentlogic.quandl.client.core.domain.timeseries.data.QuandlResponse;
import com.jamonapi.MonKey;
import com.jamonapi.MonKeyImp;
import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    static class Result {

        private Date date;

        private BigDecimal value;

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public BigDecimal getValue() {
            return value;
        }

        public void setValue(BigDecimal value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Result [date=" + date + ", value=" + value + "]";
        }
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        final FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext (
                "src/main/resources/spring/cached-multithreaded-example/application-context.xml");

        final MonKey queryFunctionKey = new MonKeyImp(
            "query function called",
            TimeUnit.MILLISECONDS.toString());

        QueryBuilder outterQueryBuilder = context.getBean(QueryBuilder.class);

        Function<QueryBuilder, QuandlResponse> outterFunction = (fQueryBuilder) -> {

            Monitor monitor = MonitorFactory.start(queryFunctionKey);

            QuandlResponse result = fQueryBuilder.datasets("NASDAQOMX", "XQC")
            .data()
            .withExternalApiKey()
            .doGetAsQuandlResponse();

            monitor.stop ();

            return result;
        };

        Future<QuandlResponse> outterFuture = outterQueryBuilder.invoke(outterFunction);

        outterFuture.get();

        for (int ctr = 0; ctr < 5; ctr++) {

            QueryBuilder queryBuilder = context.getBean(QueryBuilder.class);

            log.debug("queryBuilder: " + queryBuilder);

            Function<QueryBuilder, QuandlResponse> function = (fQueryBuilder) -> {

                Monitor monitor = MonitorFactory.start(queryFunctionKey);

                QuandlResponse result = fQueryBuilder.datasets("NASDAQOMX", "XQC")
                .data()
                .withExternalApiKey()
                .doGetAsQuandlResponse();

                monitor.stop ();

                return result;
            };

            // This listener will be called a total of four times for one single request.
            queryBuilder.addQueryBuilderEventListener(
                event -> {

                    Monitor monitor = MonitorFactory.start("listener function called");

                    if (EventType.methodBegins.equals(event.getEventType())) {
                        log.info("-- event: Method begins; event: " + event);
                    } else if (EventType.preCacheCheck.equals(event.getEventType())) {
                        log.info("-- event: Pre-cache check; event: " + event);
                    } else if (EventType.cacheMiss.equals(event.getEventType())) {
                        log.info("-- event: Cache miss; event: " + event);
                    } else if (EventType.cacheHit.equals(event.getEventType())) {
                        log.info("-- event: Cache hit; event: " + event);
                    } else if (EventType.methodEnds.equals(event.getEventType())) {

                        QuandlResponse response = (QuandlResponse) event.getValue();

                        log.info("-- event: methodEnds; uri: " + queryBuilder.getEscapedURI() +
                            ", queryBuilder: " + queryBuilder + ", response: " + response.getClass());
                    } else {
                        log.info("-- event: unknown event type: " + event.getEventType() + ", event: "
                            + event);
                    }

                    monitor.stop ();

                    log.info ("monitor: " + monitor);
                }
            );

            Future<QuandlResponse> future = queryBuilder.invoke(function);
        }

        Monitor queryFunctionMonitor = MonitorFactory.getMonitor(queryFunctionKey);

        log.info ("queryFunctionMonitor: " + queryFunctionMonitor);

        Thread.currentThread().sleep(5 * 60 * 1000);

        context.close();

        System.exit(0);
    }
}
